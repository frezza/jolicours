# jolicours et ses amis

jolicours est une moulinette qui génère un pdf et un excel à partir de descriptions de cours sous format json.

joliseq est une lib permettant d'écrire en python le contenu d'un module (CM, TD, TP...), les contraintes d'enchaînements des actes pédagogiques, pour en faire un joli graphique en pdf aussi. Le programme python décrivant un module en utilisant la lib joliseq génère également un fichier json, que jolicours peut utiliser pour décrire un cours.

L'idéal est donc de concentrer la définition de cours dans des fichiers python les décrivant (via joliseq), et de baser les comptes d'HETD, HPE et autres sur les json générés, via joliseq.

Enfin, jolical est un utilitaire qui permet de voir comment se placent les journées d'enseignement dans l'année.

## Installation

Il faut que certaines libs soient présentes.

Sous ubuntu par exemple
```
sudo apt install graphviz
```	

Il vous faudra certains modules python.
```
pip3 install pycairo xlsxwriter
```

Puis vous pouvez installer chacun des 3 utilitaires.

```
cd libs
cd jolicours
python3 setup.py install --user
cd ..
cd joliseq
python3 setup.py install --user
cd ..
cd jolical
python3 setup.py install --user
cd ..
```

Attention, le module jolicours place des scripts dans `<mon home>/.local/bin`, vérifiez bien que vous pouvez lancer les commandes depuis un bash (i.e. que le `PATH` inclue ce chemin). Pour tester

```
jolicours2pdf.py
```

Ca affichera une aide, mais vous aurez confirmation que le script est accessible.

## jolicours

### Installation (si n'est pas déjà fait)

```
cd libs/jolicours
python3 setup.py install --user
```

Et les scripts `jolicours2pdf.py` et `jolicours2excel.py` sont installés, avec un package `jolicours` dont ils dépendent.

### Description d'une formation.

Une formation est un ensemble de sections. Chaque section contient une suite de modules, certains pouvant avoir lieu en parallèle. Dans un module, on trouve des unités pédagogiques, comme les cours magistraux (CM), les travaux dirigés (TD), les travaux pratiques (TP), les projets (Projet) et les examens (Exam).

#### Unités pédagogiques (CM, TD, ...)

Elles sont décrites par une liste, ou même un seul nombre

```
3.5
[3.5]
[3.5, 2]
[3.5, "toto", 2, "titi"]
[3.5, 2, "toto", "forfait=3"]
[3.5, 2, "toto", "face2face=3"]
[3.5, 4, "toto", "toto", "titi"]
[3.5, 4, "@toto{titi}{tutu}"]
```

Le premier nombre de la liste (ou simplement le nombre), c'est la durée de l'unité pédagogique, en heure. Le second nombre (un entier), c'est le nombre de fois où cette unité est dupliquée (par défaut, 1). Ensuite, les string sont les noms de profs. Il ne peut pas y en avoir plus que de duplications, mais il peut y en avoir moins. Dans ce cas, jolicours affecte l'unité à un prof "à définir". Un même prof peut faire les deux duplications, auquel cas il doit apparaître deux fois. Par exemple, la dernière ligne désigne une unité de 3.5h, donnée en 4 instances, dont deux sont assurées par "toto", une par "titi"... et donc une par le prof à définir.

Notons que parmi les chaînes de caractères, on peut mettre "forfait=x". Il ne s'agit alors pas d'un prof, mais de la mention d'un comptage forfaitaire de l'unité pédagogique dans le service des profs. Dans ce cas, tout enseignant participant à l'unité pédagogique émarge à x HETD pour sa participation, indépendemment de la durée de l'unité.

De même, "face2face=x" déclare le nombre d'heures de l'enseignement que les élèves passent en face à face, sur la durée totale de l'unité. Par défaut, c'est la même valeur que la durée de l'unité.

Enfin, parmi les chaînes de caractère, on peut avoir une syntaxe similaire aux commandes latex (avec @ au lieu du backslash), comme "@toto{titi}{}". Il s'agit d'un enseignant, et c'est le nom de la commande qui apparaîtra dans le comptage des heures. Par exemple, "@doctorant{Michel Choupauvert}{CNRS}" et "@doctorant{Gui Liguili}{INRIA}" sont considérés dans les calculs comme le même enseignant "doctorant".



Le premier nombre de la liste (ou simplement le nombre), c'est la durée de l'unité pédagogique, en heure. Le second nombre (un entier), c'est le nombre de fois où cette unité est dupliquée (par défaut, 1). Ensuite, les string sont les noms de profs. Il ne peut pas y en avoir plus que de duplications, mais il peut y en avoir moins. Dans ce cas, jolicours affecte l'unité à un prof "à définir". Un même prof peut faire les deux duplications, auquel cas il doit apparaître deux fois. Par exemple, la dernière ligne désigne une unité de 3.5h, donnée en 4 instances, dont deux sont assurées par "toto", une par "titi"... et donc une par le prof à définir.

Notons que parmi les chaînes de caractères, on peut mettre "forfait=x". Il ne s'agit alors pas d'un prof, mais de la mention d'un comptage forfaitaire de l'unité pédagogique dans le service des profs. Dans ce cas, tout enseignant participant à l'unité pédagogique émarge à x HETD pour sa participation, indépendemment de la durée de l'unité.

Reste alors à les nommer les unités pédagogiques, on utilise pour ça un dictionnaire, dont les clés sont le nom de l'unité et la valeur la donnée décrite ci-avant.

```
{"Intro" : 3.5, "Matrices": [3.5, "toto", 2, "titi"], ...}
```

#### Les modules

Le module est un dictionnaire, dont les clés sont normalisée. Il décrivent entre autre quelles sont les unités pédagogiques de type CM, de type TD, etc... Voici l'exemple d'un module de tarot divinatoire.

```
{
  "nom"   : "Tarot",
  "name"  : "Tarot in english",
  "ECTS"  : 2, 
  "tracks": { "M-Poudlard": true, "M-Teissier": false },
  "cursus": ["formation #1", "formation #2"],
  "shared": true,
  "CM"    : {"Le jeu" : 3, "Tarot occulte" : [6, "Nostradamus"]},
  "TD"    : {"Cartes" : [1.5, 2, "Nostradamus", "Elizabeth Tessier"], "Entrailles" : [3, 4]},
  "TP"    : {"Divination" : [5, 2, "Nostradamus", "Elizabeth Tessier"]},
  "Projet" : {"Conception" : [10, "forfait=2"], "Synthèse" : [20, "forfait=3"]},
  "Exam"  : {"Oral" : 0.5, "Ecrit" : 3}
}
```


#### Les includes (aparte)

Ce concept est général dans jolicours, au sein des fichiers json qui sont manipulés. Si le texte précédent est stocké dans un fichier ```tarot.json```, on peut créer un autre ficher json où ce texte sera inclus (i.e. comme copié-collé). Il suffit d'écrire pour ce faire un petit disctionnaire avec une seule clé, "include", et la valeur un chemin relatif vers tarot.json.

```
{"include": "./modules/tarot.json"}
```

#### Les sections

Une section est une suite de cours. Elle est représentée par une liste à deux éléments ```["nom", [... liste les modules ...]]```. Les includes sont bien pratiques pour les écrire. Voici une section "semestre 1".

```
["semestre 1", [
    {"include": "../modules/toto.json"},
    {"include": "../modules/titi.json"},
    {"include": "../modules/tutu.json"},
    ...
  ]
]
```

On peut utiliser un niveau de sous-liste pour avoir des modules électifs (i.e. l'élève doit en choisir un parmis une liste de possibles).


```
["semestre 1", [
    {"include": "../modules/toto.json"},
    [{"include": "../modules/electif-1-1.json"}, {"include": "../modules/electif-1-2.json"}, ...],
    {"include": "../modules/titi.json"},
    [{"include": "../modules/electif-2-1.json"}, {"include": "../modules/electif-2-2.json"}, ...],
    {"include": "../modules/tutu.json"},
    ...
  ]
]
```
La aussi, on mettra ce texte dans un fichier ```semestre1.json```.

#### Formation

Une formation est une liste se section. C'est un dictionnaire, avec une clé "formation".

```
{"formation": [{"include": "../sections/semestre1.json"}, {"include": "../sections/semestre2.json"}, ...]}
```

On peut y ajouter à ce dictionnaire d'autres clés (normalisées aussi). Voyons la clé "respo", qui déclare des HETD de responsabilités administratives.

```
{
  "formation": [
      {"include": "../sections/semestre1.json"},
      {"include": "../sections/semestre2.json"},
      ...
    ],
  "respo" : [
      {"nom" : "Pole projet", "HETD" : 15, "profs" : ["Michel Choupauvert", "Guy Liguili"]},
      {"nom" : "1A", "HETD" : 30, "profs" : "Michel Choupauvert"}
    ]
}
```
Ici, on déclare une résponsabilité de pôle projet, chiffrée à 15 HETDs, que se partagent (équitablement) deux enseignants. On déclare la responsabilité 1A, de 30 HETD, qu'un enseignant assume seul.

#### Utilisation de jolicours

Dans la section d'exemples, on trouve des fichiers json décrivant des formations. Prenons les formations 1A, 2A et 3A. Elles contiennent des éléments identiques par facilité, mais les fichiers sont bel et bien différents (voir dossier ```modules```).

```
jolicours2pdf.py 1A ./examples/jolicours/formations/1A.json
jolicours2pdf.py 2A ./examples/jolicours/formations/2A.json
jolicours2pdf.py 3A ./examples/jolicours/formations/3A.json
```

Ces commandes génèrent ```1A.pdf```, ```2A.pdf``` et ```3A.pdf```, qui chiffrent chacune des 3 formations.

On peut aussi tout regrouper dans un fichier 'all.pdf'.

```
jolicours2pdf.py all ./examples/jolicours/formations/1A.json ./examples/jolicours/formations/2A.json ./examples/jolicours/formations/3A.json
```

Il suffit d'utiliser la commande ```jolicours2excel.py``` de la même façon pour générer le tableau excel des services associés à la formation, dans ```all.xlsx``` ici. Attention, pensez bien à forcer les calculs (cntl+F9) dans le document excel.

```
jolicours2excel.py all ./examples/jolicours/formations/1A.json ./examples/jolicours/formations/2A.json ./examples/jolicours/formations/3A.json
```

On peut aussi résoudre les includes pour obtenir un fichier de description json ```all.json```.

```
jolicours2json.py all ./examples/jolicours/formations/1A.json ./examples/jolicours/formations/2A.json ./examples/jolicours/formations/3A.json
```

## joliseq


### Installation (si n'est pas déjà fait)

```
cd libs/joliseq
python3 setup.py install --user
```

Le package `joliseq` est installé. Il permet d'écrire des fichiers de description de cours.

###  Utilisation de joliseq

Regardons un exemple.

```
cd exemples/joliseq
```

Le fichier ```ml.py``` décrit un cours. On peut l'exécuter.

```
python3 ml.py
```

Plusieurs fichiers sont générés. Les fichiers ```tmp-*``` recensent les enchaînements, les bilans d'heures, etc... ne les lisez pas.

Le fichier ```ml.dot``` permet de générer une visualisation pdf du cours.
```
dot -Tpdf ml.dot -o ml.pdf
```

Le fichier ```ml.json``` généré peut être inclus dans une description jolicours. C'est ce que fait le fichier ```formation.json``` de l'exemple.

On peut alors utiliser jolicours

```
jolicours2pdf.py test formation.json
jolicours2excel.py test formation.json
```

## jolical

### Installation (si n'est pas déjà fait)

```
cd libs/jolical
python3 setup.py install --user
```

###  Utilisation de jolical

Il suffit d'écrire un fichier python et de récupérer le .xlsx que produit son exécution.

Le principe est que l'on crée un calendrier. A la construction, on lui passe une liste de filtres, i.e. des fonctions qui disent si un jour est utilisable. A chaque filtre est associé une couleur. Si un filtre empêche qu'un jour soit utilisable, sa couleur apparaît, pour ce jour, dans le calendrier.

Quand les filtres sont posés, il suffit de se positionner à une date de démarrage, et de remplir n journées de cours (emplace). Ces remplissages sont assortis de couleurs également, afin que les journées posées apparaissent dans le calendrier.

Le remplissage de n jours remplit les n jours à partir de la date courante (incluse), en sautant les jours non utilisables. Le calendrier est alors positionné sur la prochaine date utilisable.

Des fonctions permettent d'avancer dans le calendrier sans le remplir (move_to, next_tuesday, ...).

Essayez les exemples...


## Image Docker

Si vous souhaitez essayer jolicours et joliseq sans installation, utilisez l'image Docker [gitlab-research.centralesupelec.fr:4567/frezza/jolicours/jolicours](gitlab-research.centralesupelec.fr:4567/frezza/jolicours/jolicours)

Exemple : si vos fichiers décrivant la formation sont dans votre dossier ```/MesFichiers``` tapez :

```
docker run -it --rm --mount type=bind,source=/MesFichiers,destination=/tmp gitlab-research.centralesupelec.fr:4567/frezza/jolicours/jolicours bash
```

Une fois le container démarré, placez-vous dans le dossier ```/tmp``` où est monté votre dossier de formation :

```
cd /tmp
```

Vous pouvez ensuite utiliser les commandes comme expliqué dans les sections précédentes.

Lorsque vous avez terminé, tapez simplement ```exit``` pour quitter le container et vous retrouverez vos fichiers générés dans votre dossier ```/MesFichiers```.


