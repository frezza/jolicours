#!/usr/bin/env python3
# coding: utf-8


import sys
import json
import jolicours


if len(sys.argv) < 3 :
    print('Usage : {} <output-prefix> file1.json file2.json ...'.format(sys.argv[0]))
    sys.exit(1)
try :
    print()
    print()
    prefix = sys.argv[1]
    json_name  = prefix + '.json'

    data_list, respo_list, tracks_dict = jolicours.files.read_files(sys.argv[2:])
    print('Reading json files completed.')
    print()
    result = {'formation': data_list}
    if len(respo_list) > 0:
        result['respo'] = respo_list
    if len(tracks_dict) > 0:
        result['tracks'] = tracks_dict

    with open(json_name, 'w', encoding='utf-8') as jsonfile:
        json.dump(result, jsonfile, ensure_ascii=False)
    
    
    
    
except json.decoder.JSONDecodeError as exc:
    print(exc)
except FileNotFoundError as exc:
    print(exc)
