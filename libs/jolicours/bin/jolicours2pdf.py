#!/usr/bin/env python3
# coding: utf-8


import sys
import json
import jolicours


if len(sys.argv) < 3 :
    print('Usage : {} <output-prefix> file1.json file2.json ...'.format(sys.argv[0]))
    sys.exit(1)
try :
    print()
    print()
    prefix = sys.argv[1]
    json_name  = prefix + '.json'
    pdf_name   = prefix + '.pdf'
    
    data_list, respo_list, tracks_dict = jolicours.files.read_files(sys.argv[2:])
    print('Reading json files completed.')
    print()

    shapes, tfin, endline = jolicours.layout.place_all(data_list)
    jolicours.drawings.draw(shapes, tfin, endline, pdf_name, tracks_dict)

    
except json.decoder.JSONDecodeError as exc:
    print(exc)
except FileNotFoundError as exc:
    print(exc)
