
import setuptools

setuptools.setup(
    name = "jolicours",
    version = "1.0",
    scripts = ['bin/jolicours2pdf.py', 'bin/jolicours2excel.py', 'bin/jolicours2json.py'],
    packages = setuptools.find_packages(),
    install_requires=['pycairo', 'xlsxwriter'],
)
