# coding: utf-8
import cairo

class Params:
    font = 'Ubuntu sans'

    # This is related to the lines for a module.
    colors = {'CM':(0.5, 0.5, 1), 'TD':(1, .9, 0.5), 'TP':(0.5, 0.5, 0.5), 'Projet': (.355, .67, .512), 'Exam':(1.,.6,.38)}
    pedag_kinds = colors.keys()
    main_title_color = (0.80, 0.50, 0.60)
    
    line_size       = 10
    line_skip       = .5
    hour_size       =  1.2  # width of an hour
    hour_skip       = .2    # separation between 2 hours.
    sep_color_coef  = .2    # The coef modifying the color for edges.
    title_color_coef = .5   # The darkening of the font within the blocks.
    minimum_duration = 10   # minimal width of the blocks.

    # big titles layout
    title_xoffset = 28
    title_fontsize = 4
    lineskip_section = 1.2
    line_title_ratio = .3
    lineskip_lecture = .7

    # Grid stuff
    grid_background = (0.95, 0.95, 0.95)
    grid_foreground = (0.90, 0.90, 0.90)
    graduation_start_ratio = .26


    # sections
    section_header  = 4
    section_skip    = section_header + 2
    

    # Adjusts the position of the small x2 tags.
    nb_duplicate_ycoef = .3
    nb_duplicate_xoffset = .7

    line_title_ratio = .3   # The title height relative to the full block height.

    yoffsets = [.4, .8]     # The alternating heights of module titles. 

    
    def compute_slot_yrange(y, dy, key):
        """
        Returns y and dy for the specific line.
        """
        ddy = Params.line_title_ratio*dy
        dddy = (dy - ddy)/len(Params.colors)
        yoffsets = {k: i * dddy for i, k in enumerate(Params.colors)}
        return y + ddy + yoffsets[key], dddy

    def duration2dx_single(d):
        return d*(Params.hour_size + Params.hour_skip)
        
    def duration2dx_all(d):
        if d == 0:
            return 0
        return Params.duration2dx_single(d) - Params.hour_skip

    def set_pedag_title_font(ctx):
        ctx.select_font_face(Params.font,
                             cairo.FONT_SLANT_NORMAL,
                             cairo.FONT_WEIGHT_BOLD);
        ctx.set_font_size(.5);

    def set_module_title_font(ctx):
        ctx.select_font_face(Params.font,
                             cairo.FONT_SLANT_NORMAL,
                             cairo.FONT_WEIGHT_BOLD);
        ctx.set_font_size(1);
        ctx.set_source_rgb(0,0,0)
        
    def set_main_title_font(ctx):
        ctx.select_font_face(Params.font,
                             cairo.FONT_SLANT_NORMAL,
                             cairo.FONT_WEIGHT_NORMAL);
        ctx.set_font_size(1);
        ctx.set_source_rgb(*(Params.main_title_color))
        
    def set_main_title_font_shared(ctx):
        ctx.select_font_face(Params.font,
                             cairo.FONT_SLANT_ITALIC,
                             cairo.FONT_WEIGHT_NORMAL);
        ctx.set_font_size(.75);
        ctx.set_source_rgb(*(Params.main_title_color))
        
