
# HETD liées à une responsabilité.
class Respo:

    def __str__(self) :
        res =  '[{}, {}h, by '.format(self.name, self.hetd)
        if len(self.who) == 0 :
            res += 'nobody]'
        else :
            res += '"{}"'.format(self.who[0])
            for name in self.who[1:] :
                res += ' and "{}"'.format(name)
            res += ']'
        return res
        
    def __init__(self, data) :
        self.who  = data['profs']
        if not isinstance(self.who, list):
            self.who = [self.who]
        self.name = data['nom']
        self.hetd = data['HETD']
        self.hetd_by_teacher = self.hetd/float(len(self.who))
