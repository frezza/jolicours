# coding: utf-8

from . import params
from . import hours
    
def place_stuff(title, data, shapes, begin, end) :
    """
    data :  contenu d'une shape (section ou cours).
    
    ['CM: 39.0, TD: 6.0, TP: 16.0, Projet: 0.0, Exam: 8.0', 'HPE: 69.0, HETD: 220.5, ECTS: 22.0', 'sharing HPE: 40.0, HETD: 79.0, ECTS: 17.0']

    ou 

    {'nom': 'Mathématiques fondamentales', 'ECTS': 5, 'shared': ['L3', 'Certificate'], 'CM': {'Intro': 3, 'Equadiff': [3, 'John Duff'], 'Entiers': [6, 'John Duff']}, 'TD': {'Compter': [1.5, 2, 'John Duff'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}, 'Exam': {'Ecrit': 2}, 'Projet_e': 0}
    """
    shapes.append((begin, end, title, data))

def skip_pos(data, baseline, tdeb) :
    return (baseline + params.Params.line_size + params.Params.line_skip,  tdeb + params.Params.duration2dx_single(hours.duration(data)))


def place(title, data, shapes, baseline, tdeb, parallel) :
    endline = baseline
    tfin    = tdeb
    l       = baseline
    t       = tdeb
    

    if isinstance(data, list) :
        if parallel : 
            for d in data :
                (endline, t) = place(title, d, shapes, endline, tdeb, False)
                tfin = max(t, tfin)
        else :
            for d in data :
                (l, tfin) = place(title, d, shapes, baseline, tfin, True)
                endline = max(l, endline)
    else :
        (endline, tfin) = skip_pos(data, baseline, tdeb)
        place_stuff(title, data, shapes, (baseline, tdeb), (endline - params.Params.line_skip, tfin - params.Params.hour_skip))
            
    return endline, tfin

def place_all(data_list):
    """
    data_list : the 'formation' entries of the json file.

    [['Semestre 5', 
        [{'nom': 'Mathématiques fondamentales', 'ECTS': 5, 'shared': ['L3', 'Certificate'], 'CM': {'Intro': 3, 'Equadiff': [3, 'John Duff'], 'Entiers': [6, 'John Duff']}, 'TD': {'Compter': [1.5, 2, 'John Duff'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}, 'Exam': {'Ecrit': 2}}, 
         {'nom': 'Physique', 'ECTS': 5, 'CM': {'Intro': 3, 'Thermodynamique': [12, 'Sophie Fonfec'], 'Forces': [6, 'Sophie Fonfec']}, 'TP': {'Ebullition': [3, 2, 'John Duff', 'Sophie Fonfec'], 'Plasmas': [3, 2, 'Sophie Fonfec', 'Sophie Fonfec']}, 'Exam': {'Ecrit': 2}},  ...]],
     ...]

    """
    shapes = []
    endline, tfin = params.Params.section_header, 0
    for title, data in data_list :
        place_stuff(title, hours.global_duration(data).to_lines(True), shapes, (endline - params.Params.section_header, 0), (0,0))
        e, t = place(title, data, shapes, endline, 0, False)
        endline = e + params.Params.section_skip
        tfin = max(t, tfin)
    endline -= params.Params.section_skip
    return shapes, tfin, endline
    
