# coding: utf-8

# This reads different json stuff about a pedagogical act (CM, TD,
# ...) and stored the elements in a class instance.
class Pedag:

    noteacher = 'x'
    verbose   = False

    def __str__(self) :
        res =  '[{}, {}h, by '.format(self.name, self.hetd)
        if len(self.who) == 0 :
            res += 'nobody]'
        else :
            res += '"{}"'.format(self.who[0])
            for name in self.who[1:] :
                res += ' and "{}"'.format(name)
            res += ']'
        return res

    def _read_kwd(arg, kwd):
        tokens = [s.strip() for s in arg.split('=')]
        if len(tokens) < 2:
            return None
        if tokens[0] != kwd:
            return None
        return float(tokens[1])
    
    def _read_forfait(arg):
        return Pedag._read_kwd(arg, 'forfait')
    
    def _read_face2face(arg):
        return Pedag._read_kwd(arg, 'face2face')


    def _read_teacher(arg):
        if arg[0] != '@':
            return arg
        return arg[1:].split('{')[0].split()[0]

        
    
    def __init__(self, name, data) :
        """
        [1.5, 2, 'John Duff', 'Michel Choupauvert', 'forfait=3', 'face2face=2', '@command{}{}...']
        """ 
        self.hetd_by_teacher = 0
        self.hetd = 0
        self.nb = 1
        self.who = []
        self.duration = 0
        self.name = name
        self.forfait = None
        self.face2face = None

        if isinstance(data, int) :
            self.duration = float(data)
            self.hetd     = self.duration
  
        elif isinstance(data, float) :
            self.duration = data
            self.hetd     = self.duration

        elif isinstance(data, list) :
            nb_values = 0
            for elem in data :
                if isinstance(elem, float) or isinstance(elem, int) :
                    if nb_values == 0 :
                        self.duration = float(elem)
                    elif nb_values == 1 :
                        self.nb = int(elem)
                    else :
                        print('error : too many numbers in pedagogical act "{}" (extra {} found)'.format(self.title, elem))
                    nb_values += 1
                elif isinstance(elem, str) :
                    forfait = Pedag._read_forfait(elem)
                    if forfait is None:
                        face2face = Pedag._read_face2face(elem)
                        if face2face is None:
                            self.who.append(Pedag._read_teacher(elem))
                        else:
                            self.face2face = face2face
                    else:
                        self.forfait = forfait
            if self.forfait is None:
                self.hetd = self.duration * self.nb
            else:
                self.hetd = self.forfait * self.nb
            
        else:
            raise ValueError(f'pedag.__init__() : bad data type : {data}')

        nb = len(self.who)
        if nb > self.nb :
            if Pedag.verbose :
                print('###  Error  ### : Too many teachers in {}.'.format(self))
        elif nb < self.nb :
            add = self.nb - nb
            self.who += ([Pedag.noteacher]*add)
            if Pedag.verbose :
                print('--- warning --- : adding {:3} "x" teachers : {}.'.format(add, self))

        self.hetd_by_teacher = self.hetd/self.nb

            
         
