
import xlsxwriter

from . import service
from . import respo
from . import pedag
from . import hours

class Excel:
    def __init__(self, shapes, respos, tracks_dict, filename):
        self.workbook = xlsxwriter.Workbook(filename)
        self.srv = {}
        for title, s in ((shape[2], shape[3]) for shape in shapes if isinstance(shape[3], dict)) :
            service.read_module(self.srv, s['nom'], title, s)
        for r in (respo.Respo(rsp) for rsp in respos) :
            service.read_respo(self.srv, r)

        self.setup_page()
        self.make_total()
        self.make_parcours(shapes, tracks_dict)
        self.make_teachers()

        self.workbook.close()
        print('file "{}" generated.'.format(filename))

    def setup_page(self):
        self.title1   = self.workbook.add_format({'bg_color': '#cccccc', 'bold': True, 'align': 'center'})
        self.title2   = self.workbook.add_format({'bg_color': '#eeeeee', 'bold': True, 'align': 'center'})
        self.title3   = self.workbook.add_format({'bg_color': '#cccccc', 'bold': True, 'align': 'right'})
        self.title4   = self.workbook.add_format({'bg_color': '#eeeeee', 'bold': True, 'align': 'left'})
        self.label    = self.workbook.add_format({'align': 'left'})
        self.check    = self.workbook.add_format({'align': 'center', 'bold': True})
        self.checkgreen  = self.workbook.add_format({'align': 'center', 'bold': True, 'bg_color': '#ddffdd'})
        self.checkred    = self.workbook.add_format({'align': 'center', 'bold': True, 'bg_color': '#ffdddd'})
        self.hour     = self.workbook.add_format({'num_format': '0.00', 'align': 'right'})
        self.boldhour = self.workbook.add_format({'bold': True, 'num_format': '0.00', 'align': 'right'})

        self.col_title  = 1
        self.col_module = self.col_title + 1
        self.col_label  = self.col_title + 2
        self.col_cm     = self.col_title + 3
        self.col_td     = self.col_title + 4
        self.col_tp     = self.col_title + 5
        self.col_projet = self.col_title + 6
        self.col_exam   = self.col_title + 7
        self.col_respo  = self.col_title + 8
        
        self.first_col  = self.col_title
        self.last_col   = self.col_respo
        self.line_title  = 1
        self.line_header = 2
    
        self.line_base_activity = self.line_header + 1


        self.col_of_kind = {'CM': self.col_cm, 'TD': self.col_td, 'TP': self.col_tp,
                            'Projet': self.col_projet, 'Exam': self.col_exam, 'Respo': self.col_respo}

    def make_total(self):
        # builds the total page, formulas for each row will be generated after.
        self.worksheet_total = self.workbook.add_worksheet('total')
        self.total_ws_lines = dict()
        total_ws_line       = 2
        total_ws_line_title = 1
        total_ws_name_col   = 1
        total_ws_base_col   = 2
        self.total_ws_hetd_col   = total_ws_base_col + 1
        self.total_ws_cm_col     = total_ws_base_col + 3
        self.total_ws_td_col     = total_ws_base_col + 4
        self.total_ws_tp_col     = total_ws_base_col + 5
        self.total_ws_projet_col = total_ws_base_col + 6
        self.total_ws_respo_col  = total_ws_base_col + 7
        self.worksheet_total.write(total_ws_line_title, self.total_ws_hetd_col, "HETD", self.title1)
        self.worksheet_total.write(total_ws_line_title, self.total_ws_cm_col, "CM", self.title1)
        self.worksheet_total.write(total_ws_line_title, self.total_ws_td_col, "TD", self.title1)
        self.worksheet_total.write(total_ws_line_title, self.total_ws_tp_col, "TP", self.title1)
        self.worksheet_total.write(total_ws_line_title, self.total_ws_projet_col, "Projet", self.title1)
        self.worksheet_total.write(total_ws_line_title, self.total_ws_respo_col, "Responsabilité", self.title1)
        
        first_line = total_ws_line
        
        for teacher in self.srv :
            if teacher != pedag.Pedag.noteacher :
                self.total_ws_lines[teacher] = total_ws_line
                total_ws_line += 1
        self.total_ws_lines[pedag.Pedag.noteacher] = total_ws_line

        last_line = total_ws_line
        total_line = last_line + 2

        for (teacher, line)  in self.total_ws_lines.items() :
            self.worksheet_total.write(line, total_ws_name_col, teacher, self.title4)
            
        self.worksheet_total.write(total_line, total_ws_name_col, 'TOTAL', self.title4)
        total_cols = [self.total_ws_hetd_col,
                      self.total_ws_cm_col,
                      self.total_ws_td_col,
                      self.total_ws_tp_col,
                      self.total_ws_projet_col,
                      self.total_ws_respo_col]
        for col in total_cols:
            self.worksheet_total.write_formula(total_line, col,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first_line, col, last_line, col)),
                                    self.boldhour)


    def make_parcours(self, shapes, tracks_dict):
        worksheet_parcours = self.workbook.add_worksheet('Parcours')
    
        parcours_line_title    = 1
        parcours_line_subtitle = 2
        
        parcours_section_col    = 1
        parcours_name_col    = 2
        parcours_hpe_col     = 3
        parcours_ects_col    = 4
        parcours_classic_col = parcours_ects_col+2
        parcours_track_col = dict()
        parcours_first_track_col = parcours_classic_col
        parcours_last_track_col  = parcours_classic_col + len(tracks_dict)

        for i, k in enumerate(tracks_dict) :
            parcours_track_col[k] = parcours_classic_col + i + 1

        worksheet_parcours.merge_range(parcours_line_title, parcours_section_col, parcours_line_title, parcours_ects_col, "Modules", self.title1)

        worksheet_parcours.merge_range(parcours_line_title, parcours_first_track_col, parcours_line_title, parcours_last_track_col, "Parcours", self.title1)

        worksheet_parcours.write(parcours_line_subtitle, parcours_section_col, '', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_name_col, '', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_hpe_col, 'HPE', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_ects_col, 'ECTS', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_classic_col, 'Classique', self.title2)
        for tag, col in parcours_track_col.items() :
            worksheet_parcours.write(parcours_line_subtitle, col, tracks_dict[tag]['name'], self.title2)


        def check_classic(baseline, coursp):
            max_value = -1
            max_l = None
            for l, c in enumerate(coursp):
                val = hours.global_duration(c).data['HPE']
                if val > max_value :
                    max_value = val
                    max_l = l
            worksheet_parcours.write(baseline + max_l, parcours_classic_col, 1, self.check)
        
        
        def check_in_track(baseline, col, cours_parallels, tag) :
            trues  = []
            falses = []
            for l, cours in enumerate(coursp):
                if 'tracks' in cours :
                    if tag in cours['tracks'] :
                        if cours['tracks'][tag] :
                            trues.append(l)
                        else:
                            falses.append(l)

            for l in falses :
                worksheet_parcours.write(baseline + l, col, '', self.checkred)

            if len(trues) == 0 :
                max_value = -1
                max_l = None
                for l, c in enumerate(coursp):
                    if l not in falses:
                        val = hours.global_duration(c).data['HPE']
                        if val > max_value :
                            max_value = val
                            max_l = l
                if max_l != None :
                    worksheet_parcours.write(baseline + max_l, col, 1, self.check)
            else:
                max_value = -1
                max_l = None
                for l, c in enumerate(coursp):
                    if l in trues:
                        val = hours.global_duration(c).data['HPE']
                        if val > max_value :
                            max_value = val
                            max_l = l
                worksheet_parcours.write(baseline + max_l, col, 1, self.checkgreen)
                if l in trues :
                    if l != max_l :
                        worksheet_parcours.write(baseline + l, col, '', self.checkgreen)
                
            
            
        cours_paralleles = dict()
        for (draw_col, section, cours) in ((shape[0][1], shape[2], shape[3]) for shape in shapes if isinstance(shape[3], dict)):
            key = (section, draw_col)
            if key not in cours_paralleles :
                cours_paralleles[key] = []
            cours_paralleles[key].append(cours)
    
        parcours_line_cours = parcours_line_subtitle + 1
        parcours_first_line_cours = parcours_line_cours
        for (key, coursp) in cours_paralleles.items() :
            for l, cours in enumerate(coursp) :
                if l == 0 :
                    worksheet_parcours.write(parcours_line_cours + l, parcours_section_col, key[0], self.title4)
                else:
                    worksheet_parcours.write(parcours_line_cours + l, parcours_section_col, '', self.title4)
                worksheet_parcours.write(parcours_line_cours + l, parcours_name_col, cours['nom'], self.title4)
                dur = hours.global_duration(cours)
                worksheet_parcours.write(parcours_line_cours + l, parcours_hpe_col, dur.data['HPE'], self.hour)
                worksheet_parcours.write(parcours_line_cours + l, parcours_ects_col, dur.data['ECTS'], self.hour)
            check_classic(parcours_line_cours, coursp)
            for tag, col in parcours_track_col.items() :
                check_in_track(parcours_line_cours, col, coursp, tag)
            parcours_line_cours += len(coursp)
        parcours_last_line_cours  = parcours_line_cours-1

        parcours_track_col        = parcours_last_track_col + 2
        parcours_track_hpe_col    = parcours_track_col + 1
        parcours_track_hpeout_col = parcours_track_col + 2
        parcours_track_tothpe_col = parcours_track_col + 3
        parcours_track_suphpe_col = parcours_track_col + 4
        parcours_track_ects_col   = parcours_track_col + 5
        parcours_track_classic_line = parcours_line_subtitle + 1

        worksheet_parcours.merge_range(parcours_line_title, parcours_track_col, parcours_line_title, parcours_track_ects_col, 'Bilan', self.title1)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_col, '', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_hpe_col, 'HPE-CS', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_hpeout_col, 'HPE-nonCS', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_tothpe_col, 'total HPE', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_suphpe_col, 'suppl. HPE', self.title2)
        worksheet_parcours.write(parcours_line_subtitle, parcours_track_ects_col, 'ECTS', self.title2)
    
        parcours_line_cours = parcours_track_classic_line
        col = parcours_first_track_col
        worksheet_parcours.write_formula(parcours_line_cours, parcours_track_col,
                                         '={}'.format(xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_subtitle, col)),
                                         self.title4)
        worksheet_parcours.write_formula(parcours_line_cours, parcours_track_tothpe_col,
                                         '={}'.format(xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_cours, parcours_track_hpe_col)),
                                        self.hour)
        parcours_line_cours += 1
        col += 1
        for i, k in enumerate(tracks_dict) :
            worksheet_parcours.write_formula(parcours_line_cours, parcours_track_col,
                                             '={}'.format(xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_subtitle, col)),
                                             self.title4)
            parcours_line_cours += 1
            col += 1
        
        parcours_line_cours = parcours_track_classic_line
        col = parcours_first_track_col
        worksheet_parcours.write_formula(parcours_line_cours, parcours_track_hpe_col,
                                         '=SUMPRODUCT({},{})'.format(
                                             xlsxwriter.utility.xl_range(parcours_first_line_cours, col, parcours_last_line_cours, col),
                                             xlsxwriter.utility.xl_range(parcours_first_line_cours, parcours_hpe_col, parcours_last_line_cours, parcours_hpe_col)),
                                         self.hour)
        worksheet_parcours.write_formula(parcours_line_cours, parcours_track_ects_col,
                                         '=SUMPRODUCT({},{})'.format(
                                             xlsxwriter.utility.xl_range(parcours_first_line_cours, col, parcours_last_line_cours, col),
                                             xlsxwriter.utility.xl_range(parcours_first_line_cours, parcours_ects_col, parcours_last_line_cours, parcours_ects_col)),
                                         self.hour)
        parcours_line_cours += 1
        col += 1
        for i, name_track in enumerate(tracks_dict.items()) :
            worksheet_parcours.write(parcours_line_cours, parcours_track_hpeout_col, name_track[1]['HPE'], self.hour)
            worksheet_parcours.write_formula(parcours_line_cours, parcours_track_hpe_col,
                                             '=SUMPRODUCT({},{})'.format(
                                                 xlsxwriter.utility.xl_range(parcours_first_line_cours, col, parcours_last_line_cours, col),
                                                 xlsxwriter.utility.xl_range(parcours_first_line_cours, parcours_hpe_col, parcours_last_line_cours, parcours_hpe_col)),
                                             self.hour)
            worksheet_parcours.write_formula(parcours_line_cours, parcours_track_ects_col,
                                             '=SUMPRODUCT({},{})'.format(
                                                 xlsxwriter.utility.xl_range(parcours_first_line_cours, col, parcours_last_line_cours, col),
                                                 xlsxwriter.utility.xl_range(parcours_first_line_cours, parcours_ects_col, parcours_last_line_cours, parcours_ects_col)),
                                             self.hour)
            worksheet_parcours.write_formula(parcours_line_cours, parcours_track_tothpe_col,
                                             '={}+{}'.format(xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_cours, parcours_track_hpe_col),
                                                             xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_cours, parcours_track_hpeout_col)),
                                             self.hour)
            worksheet_parcours.write_formula(parcours_line_cours, parcours_track_suphpe_col,
                                             '={}-{}'.format(xlsxwriter.utility.xl_rowcol_to_cell(parcours_line_cours, parcours_track_tothpe_col),
                                                             xlsxwriter.utility.xl_rowcol_to_cell(parcours_track_classic_line, parcours_track_tothpe_col)),
                                             self.hour)
            parcours_line_cours += 1
            col += 1
    

    def make_teachers(self):

        worksheet_noteacher = self.workbook.add_worksheet(pedag.Pedag.noteacher)
        for teacher, activities in self.srv.items() :
            if teacher == pedag.Pedag.noteacher :
                worksheet = worksheet_noteacher
            else :
                worksheet = self.workbook.add_worksheet(teacher)

            worksheet.merge_range(self.line_title, self.first_col, self.line_title, self.last_col, teacher, self.title1)
            worksheet.write(self.line_header, self.col_cm, 'CM', self.title2)
            worksheet.write(self.line_header, self.col_td, 'TD', self.title2)
            worksheet.write(self.line_header, self.col_tp, 'TP', self.title2)
            worksheet.write(self.line_header, self.col_projet, 'Projet', self.title2)
            worksheet.write(self.line_header, self.col_exam, 'Exam', self.title2)
            worksheet.write(self.line_header, self.col_respo, 'Responsabilité', self.title2)
            for num, activity in enumerate(activities) :
                worksheet.write(self.line_base_activity + num, self.col_title,  activity[4], self.label)
                worksheet.write(self.line_base_activity + num, self.col_module, activity[0], self.label)
                worksheet.write(self.line_base_activity + num, self.col_label,  activity[2], self.label)
                worksheet.write(self.line_base_activity + num, self.col_of_kind[activity[1]], activity[3], self.hour)
            first      = self.line_base_activity
            last       = self.line_base_activity + len(activities) - 1
            line_total = last + 2
            worksheet.merge_range(line_total, self.first_col, line_total, self.col_label, 'Total', self.title3)
            worksheet.write_formula(line_total, self.col_cm,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_cm, last, self.col_cm)),
                                    self.boldhour)
            worksheet.write_formula(line_total, self.col_td,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_td, last, self.col_td)),
                                    self.boldhour)
            worksheet.write_formula(line_total, self.col_tp,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_tp, last, self.col_tp)),
                                    self.boldhour)
            worksheet.write_formula(line_total, self.col_projet,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_projet, last, self.col_projet)),
                                    self.boldhour)
            worksheet.write_formula(line_total, self.col_exam,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_exam, last, self.col_exam)),
                                    self.boldhour)
            worksheet.write_formula(line_total, self.col_respo,
                                    '=SUM({})'.format(xlsxwriter.utility.xl_range(first, self.col_respo, last, self.col_respo)),
                                    self.boldhour)
            line_hetd = line_total+1
            worksheet.merge_range(line_hetd, self.first_col, line_hetd, self.col_label, 'HETD', self.title1)
            worksheet.write_formula(line_hetd, self.col_cm,
                                    '='+ hours.Duration.formula_hetd(xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_cm),
                                                              xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_td),
                                                              xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_tp),
                                                              xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_projet),
                                                              xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_exam),
                                                              xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_respo)),
                                    self.title2)
            
            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_hetd_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_hetd, self.col_cm)), # Yes, col_cm
                                               self.hour)
            
            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_cm_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_cm)),
                                               self.hour)
            
            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_td_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_td)),
                                               self.hour)
            
            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_tp_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_tp)),
                                               self.hour)
            
            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_projet_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_projet)),
                                               self.hour)

            self.worksheet_total.write_formula(self.total_ws_lines[teacher], self.total_ws_respo_col,
                                               "='{}'!{}".format(teacher, xlsxwriter.utility.xl_rowcol_to_cell(line_total, self.col_respo)),
                                               self.hour)




