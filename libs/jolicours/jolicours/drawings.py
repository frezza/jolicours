# coding: utf-8
import cairo

from . import hours
from . import pedag
from . import params




def lightening(color, coef) :
    """
    returns darken,lighten colors.
    """
    ccoef = 1 - coef
    return ((       color[0]*ccoef,        color[1]*ccoef,        color[2]*ccoef),
            (coef + color[0]*ccoef, coef + color[1]*ccoef, coef + color[2]*ccoef))



def draw_edge(ctx, color, x_range, y_range) :
    """
    Draws a vertical separation between two color blocks.
    """
    x  = x_range[0]
    dx = .5*(x_range[1] - x)
    y  = y_range[0]
    dy = y_range[1] - y
    dark, light = lightening(color, params.Params.sep_color_coef)
    ctx.set_source_rgb(dark[0], dark[1], dark[2])
    ctx.rectangle(x, y, dx, dy)
    ctx.fill()

    x += dx
    ctx.set_source_rgb(light[0], light[1], light[2])
    ctx.rectangle(x, y, dx, dy)
    ctx.fill()

def draw_border(ctx, x, y, dx, dy, color) :
    """
    Draws the edges around a block.
    """
    dark, light = lightening(color, params.Params.sep_color_coef)
    ctx.set_source_rgb(light[0], light[1], light[2])
    ctx.rectangle(x, y, params.Params.hour_skip*.5, dy)
    ctx.fill()
    ctx.set_source_rgb(dark[0], dark[1], dark[2])
    ctx.rectangle(x+dx-params.Params.hour_skip*.5, y, params.Params.hour_skip*.5, dy)
    ctx.fill()

def draw_lecture_key(ctx, s, key, x, y, dy) :
    """
    For a lecture, this draws one line (CM, TD, TP...). The key tells which line to draw.
    """
    if key not in s :
        return
    yyy, dddy = params.Params.compute_slot_yrange(y, dy, key)
    color = params.Params.colors[key]
    ddx   = params.Params.duration2dx_all(hours.duration_key(s, key)[0])

    ### background
    ctx.set_source_rgb(color[0], color[1], color[2])
    ctx.rectangle(x, yyy, ddx, dddy)
    ctx.fill()
    draw_border(ctx, x, yyy, ddx, dddy, color)
    
    pedags = [pedag.Pedag(title, d) for (title, d) in s[key].items()]
    
    # edges
    if len(s[key]) > 1 :
        duration = 0
        for p in pedags[:-1] :
            duration += p.duration
            xxx = x + params.Params.duration2dx_all(duration)
            draw_edge(ctx, color, (xxx, xxx+params.Params.hour_skip), (yyy, yyy+dddy))

    # titles
    params.Params.set_pedag_title_font(ctx)
    
    title_color, _ = lightening(color, params.Params.title_color_coef)
    length = 0
    yoffset = 0
    for p in pedags :
        xxx = x + length + .2
        length += params.Params.duration2dx_single(p.duration)
        
        ctx.set_source_rgb(title_color[0], title_color[1], title_color[2])
        ctx.move_to(xxx, yyy + dddy*params.Params.yoffsets[yoffset])
        ctx.show_text(p.name)
        ctx.stroke()

        if p.nb != 1 :
            base_x = x + length - params.Params.hour_skip - params.Params.nb_duplicate_xoffset
            base_y = yyy + dddy * params.Params.nb_duplicate_ycoef
            ctx.set_source_rgb(1., 0., 0.)
            ctx.arc(base_x+.3, base_y -.15, .4, 0, 6.3)
            ctx.fill()
            ctx.set_source_rgb(1., 1., 1.)
            ctx.move_to(base_x, base_y)
            pattern = '{}'
            if p.nb < 10 :
                pattern = 'x{}'
            ctx.show_text(pattern.format(p.nb))
                
        yoffset = 1-yoffset


def draw_lectures(ctx, s, x, y, dy) :
    for key in params.Params.colors:
        draw_lecture_key(ctx, s, key,   x, y, dy)


def draw_multi_lines(ctx, x, y, dy, lines) :
    for line in lines :
        ctx.move_to(x,y)
        ctx.show_text(line)
        y += dy
        
def draw_multi_lines_with_shared(ctx, x, y, dy, lines) :
    params.Params.set_main_title_font(ctx)
    for line in lines[:-1] :
        ctx.move_to(x,y)
        ctx.show_text(line)
        y += dy
    params.Params.set_main_title_font_shared(ctx)
    y -= dy*.25
    ctx.move_to(x,y)
    ctx.show_text(lines[-1])
    

def draw_shared_symbol(ctx):
    ctx.set_source_rgb(*(params.Params.main_title_color))
    xbox, ybox = ctx.get_current_point()
    radius = .5
    ctx.arc(xbox + radius, ybox + radius, radius, 0., 2 * 3.15)
    ctx.fill()
    ctx.set_source_rgb(1, 1, 1)
    r2 = .22
    r3 = .12
    ax, ay = xbox + radius - r2, ybox + radius
    bx, by = xbox + radius + .5*r2, ybox + radius + 0.866*r2
    cx, cy = xbox + radius + .5*r2, ybox + radius - 0.866*r2
    ctx.arc(ax, ay, r3, 0., 2 * 3.15)
    ctx.fill()
    ctx.arc(bx, by, r3, 0., 2 * 3.15)
    ctx.fill()
    ctx.arc(cx, cy, r3, 0., 2 * 3.15)
    ctx.fill()
    ctx.move_to(bx, by)
    ctx.line_to(ax, ay)
    ctx.line_to(cx, cy)
    ctx.stroke()
    ctx.set_source_rgb(0, 0, 0)

def draw_bubble(ctx, text, color, striked = False):
    extra = 0.1
    xbox,ybox = ctx.get_current_point()
    _, _, width, height,_,_ = ctx.text_extents(text)
    width += extra
    height += 2 * extra
    radius = height/2 
                
    ctx.set_source_rgb(*color)
    ctx.rectangle(xbox + radius, ybox  + extra -height, width, height)
    ctx.fill()
    ctx.arc(xbox + radius, ybox + extra - radius, radius, 0., 2 * 3.15)
    ctx.fill()
    ctx.arc(xbox + radius + width, ybox + extra - radius, radius, 0., 2 * 3.15)
    ctx.fill()
                               
    ctx.set_source_rgb(1,1,1)
    ctx.move_to(xbox + radius, ybox)
    ctx.show_text(text)
    
    if(striked):
        ctx.set_source_rgba(1,0,0,.6)
        ctx.set_line_width(height/2)
        ctx.move_to(xbox + 1.5*radius, ybox)
        ctx.line_to(xbox + 0.5 * radius + width, ybox - height + 2 * extra) 
        ctx.stroke()
        
    ctx.move_to(xbox + 2*radius + width, ybox)
        
def draw_shape(ctx, shape, tracks_dict) :
    (y , x ) = shape[0]
    (yy, xx) = shape[1]
    title    = shape[2]
    s        = shape[3]
    
    if isinstance(s, list) and len(s) > 0 and isinstance(s[0], str) :
        draw_multi_lines_with_shared(ctx, x, y+params.Params.lineskip_section, params.Params.lineskip_section, s)
        params.Params.set_main_title_font(ctx)
        ctx.set_font_size(params.Params.title_fontsize);
        ctx.move_to(x+params.Params.title_xoffset,y+2.75*params.Params.lineskip_section)
        ctx.show_text(title)
        return
    
    dx = xx - x 
    dy = yy - y

    # background
    ctx.set_source_rgb(params.Params.grid_background[0], params.Params.grid_background[1], params.Params.grid_background[2])
    ctx.rectangle(x, y, dx, dy)
    ctx.fill()

    # verticalbars
    timecol = x+params.Params.hour_size
    bar_rank = 1
    while timecol+2*params.Params.hour_skip < xx :
        ctx.set_source_rgb(params.Params.grid_foreground[0], params.Params.grid_foreground[1], params.Params.grid_foreground[2])
        ctx.set_line_width(.1);
        ctx.move_to(timecol+.5*params.Params.hour_skip, y+params.Params.graduation_start_ratio*dy)
        ctx.line_to(timecol+.5*params.Params.hour_skip, y+   dy)
        ctx.stroke()
        timecol += params.Params.hour_size + params.Params.hour_skip
        bar_rank += 1

    

    # title
    params.Params.set_module_title_font(ctx)
    
    if 'shared' in s:          
        ctx.move_to(x+.1, y+.1)             
        draw_shared_symbol(ctx)
        ctx.move_to(x+1.3,y+1)
    else:
        ctx.move_to(x+.1,y+1)
    ctx.show_text(s['nom'])

    ctx.select_font_face('Ubuntu sans',
                         cairo.FONT_SLANT_NORMAL,
                         cairo.FONT_WEIGHT_NORMAL);
    ctx.set_font_size(.5);
    ctx.set_source_rgb(0,0,0)
    draw_multi_lines(ctx, x+.1,y+1.75, params.Params.lineskip_lecture, hours.print_shape_duration(s))

    # tracks (master, GTL, etc)

    if('tracks' in s):
        
        ctx.select_font_face('Ubuntu sans',
                             cairo.FONT_SLANT_NORMAL,
                             cairo.FONT_WEIGHT_BOLD);
        
        for trackName, flag in s['tracks'].items():
            if not (trackName in tracks_dict):
                print('--- !ERROR! --- : unknown track name "{}"'.format(trackName))                 
            else:
                trackDesc = tracks_dict[trackName]                
                ctx.rel_move_to(0.2,0)                
                draw_bubble(ctx, trackDesc["symbol"], trackDesc["color"], not flag)
    

    
    # lectures
    draw_lectures(ctx, s, x, y, dy)
    
    
    
def draw(shapes, width, height, filename, tracks_dict) :
    zoom = 10
    size = (zoom*width, zoom*height)
    surface = cairo.PDFSurface (filename, size[0], size[1])
    ctx = cairo.Context (surface)
    #zoomFactor = 100*min(A4landscape[0]/width, A4landscape[1]/height)
    ctx.scale(zoom, zoom)
    
    ctx.set_source_rgb(1,1,1)
    ctx.rectangle(0,0,width,height)
    ctx.fill()
    for shape in shapes :
        draw_shape(ctx, shape, tracks_dict)
    ctx.show_page()
    print('file "{}" generated.'.format(filename))

