# coding: utf-8

from pathlib import Path
import json

def parse_json(filepath):
    filepath = Path(filepath)
    print(f'Reading "{filepath}"')
    json_data  = json.load(open(filepath, 'r', encoding='utf-8'))
    current_dir = filepath.parents[0]
    return expand_includes(current_dir, json_data)

def expand_includes(current_dir, json_data):
    if isinstance(json_data, dict):
        if len(json_data) == 1 and 'include' in json_data:
            return parse_json(current_dir / Path(json_data['include']))
        return {k: expand_includes(current_dir, v) for k, v in json_data.items()}
        
    if isinstance(json_data, list):
        return [expand_includes(current_dir, v) for v in json_data]

    return json_data

def update_tracks(tracks_dict, json_data):
    if 'tracks' in json_data:
        tracks_dict.update(json_data['tracks'])

def update_respo(respo_list, json_data):
    if 'respo' in json_data:
        respo_list += json_data['respo']

def read_files(file_list):
    data_list  = []
    respo_list = []
    tracks_dict = {}
    for path in file_list:
        json_data = parse_json(path)
        data_list += (json_data['formation'])
        update_tracks(tracks_dict, json_data)
        update_respo(respo_list, json_data)
    return data_list, respo_list, tracks_dict
        
    
                

    
