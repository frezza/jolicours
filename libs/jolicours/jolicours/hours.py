# coding: utf-8

from . import params
from . import pedag

"""
Agglomère tout ce qui a une durée.
"""
class Duration:
    def compute_hetd(cm, td, tp, projet, exam, respo):
        return 1.5 * cm + td + tp + projet + respo # not exams.

    def formula_hetd(cm, td, tp, projet, exam, respo):
        return f'1.5 * {cm} + {td} + {tp} + {projet} + {respo}' # not exams.
    
    def __init__(self):
        self.data_shared = {'HPE' : 0, 'HF2F': 0, 'ECTS' : 0, 'HETD' : 0}
        self.data = {'Respo_p' : 0, 'HPE' : 0, 'HF2F': 0, 'ECTS' : 0, 'HETD' : 0}
        for tag in ['e', 'p']:
            self.data.update({f'{k}_{tag}': 0 for k in params.Params.pedag_kinds})
        
    def __iadd__(self, other):
        for k in params.Params.pedag_kinds:
            self.data[f'{k}_e'] += other.data[f'{k}_e']
        self.data['HPE']      += other.data['HPE']
        self.data['HF2F']     += other.data['HF2F']
        self.data['ECTS']     += other.data['ECTS']
        self.data['HETD']     += other.data['HETD']
        
        self.data_shared['HPE']    += other.data_shared['HPE']
        self.data_shared['HF2F']   += other.data_shared['HF2F']
        self.data_shared['ECTS']   += other.data_shared['ECTS']
        self.data_shared['HETD']   += other.data_shared['HETD']
        return self

    def update_HETD(self):
        args = [self.data[f'{k}_p'] for k in params.Params.pedag_kinds]
        args.append(self.data['Respo_p'])
        self.data['HETD'] = Duration.compute_hetd(*args)
        
    def update_HPE(self):
        hpe = 0
        for k in params.Params.pedag_kinds:
            hpe += self.data[f'{k}_e']
        self.data['HPE'] = hpe
        
        
    def update_shared(self):
        self.data_shared['HPE']  = self.data['HPE']
        self.data_shared['HF2F'] = self.data['HF2F']
        self.data_shared['ECTS'] = self.data['ECTS']
        self.data_shared['HETD'] = self.data['HETD']

    def add_prof_hours_only(self, other):
        for k in params.Params.pedag_kinds:
            self.data[f'{k}_p'] += other.data[f'{k}_p']
        self.data['HETD']      += other.data['HETD']
        
        self.data_shared['HETD'] += other.data_shared['HETD']

    def set_from_data(self, data):
        """
        data = {'nom': 'Mathématiques fondamentales', 'ECTS': 5, 'shared': ['L3', 'Certificate'], 'CM': {'Intro': 3, 'Equadiff': [3, 'John Duff'], 'Entiers': [6, 'John Duff']}, 'TD': {'Compter': [1.5, 2, 'John Duff', 'forfait=2', 'face2face=3'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}, 'Exam': {'Ecrit': 2}}
        """
        f2f = 0
        for key in ['CM', 'TD', 'TP', 'Projet', 'Exam']:
            self.data[key + '_e'], f, self.data[key + '_p'] = duration_key(data,  key)
            f2f += f
        self.data['HF2F'] = f2f
        self.data['ECTS'] = duration_key(data, 'ECTS')
        self.update_HPE()
        self.update_HETD()
        if 'shared' in data:
            self.update_shared()

    def to_lines(self, show_sharing):
        res = []
        for key in params.Params.pedag_kinds:
            key_e = key + '_e'
            if self.data[key_e] != 0:
                res.append('{}: {:.1f}'.format(key, self.data[key_e]))
        res = [', '.join(res)]
        res.append('HPE: {:.1f}, HF2F: {:.1f}, HETD: {:.1f}, ECTS: {:.1f}'.format(self.data['HPE'], self.data['HF2F'], self.data['HETD'], self.data['ECTS']))
        if show_sharing:
            if self.data_shared['HPE'] > 0 or self.data_shared['ECTS'] or self.data_shared['HETD'] > 0:
                res.append('sharing HPE: {:.1f}, HF2F: {:.1f}, HETD: {:.1f}, ECTS: {:.1f}'.format(self.data_shared['HPE'], self.data_shared['HF2F'], self.data_shared['HETD'], self.data_shared['ECTS']))
            else:
                res.append('nothing shared')
        return res


def duration_key(data, key) :
    """
        data = {'nom': 'Mathématiques fondamentales', 'ECTS': 5, 'shared': ['L3', 'Certificate'], 'CM': {'Intro': 3, 'Equadiff': [3, 'John Duff'], 'Entiers': [6, 'John Duff']}, 'TD': {'Compter': [1.5, 2, 'John Duff', 'forfait=2', 'face2face=3'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}, 'Exam': {'Ecrit': 2}}
    """
    if key in data :
        if key == 'ECTS' :
            return data['ECTS']
        d_e = 0
        d_f2f = 0
        d_p = 0
        for (title,val) in data[key].items() :
            p = pedag.Pedag(title, val)
            d_e += p.duration
            if p.face2face:
                d_f2f += p.face2face
            else:
                d_f2f += p.duration
            d_p += p.hetd
        return d_e, d_f2f, d_p
    elif key in ['ECTS']:
        return 0
    elif key in params.Params.pedag_kinds:
        return (0, 0, 0)
    else:
        raise ValueError(f'hours.duration_key(data, key) : bad key ({key}).')
    
def duration(data) :
    return max(params.Params.minimum_duration, duration_key(data, 'CM')[0], duration_key(data, 'TD')[0], duration_key(data, 'TP')[0],  duration_key(data, 'Projet')[0], duration_key(data, 'Exam')[0])


def global_duration(data, level = 0) :
    """
    data : dépend du niveau de profondeur.
    """
    res = Duration()
    if isinstance(data, list) :
        if level == 0:
            for d in data :
                res += global_duration(d, level + 1)
        elif level == 1:
            res.set_from_data(data[0])
            for d in data[1:] :
                arg = Duration()
                arg.set_from_data(d)
                res.add_prof_hours_only(arg)
        else:
            raise ValueError(f'Found list at level {level} in hours.global_duration')
    else :
        res.set_from_data(data)
    return res;

def print_shape_duration(s) :
    res = Duration()
    res.set_from_data(s)
    return res.to_lines(False)  
    
