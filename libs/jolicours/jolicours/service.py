
from . import params
from . import pedag

def read_pedag(srv, name, title, kind, p) :
    """
    name:  'Mathématiques fondamentales'
    title: 'Semestre 6'
    kind:  'TD'
    p: An instance of pedag.Pedag
    """
    for teacher in p.who :
        if teacher not in srv :
            srv[teacher] = []
        srv[teacher].append((name, kind, p.name, p.hetd_by_teacher, title))
    
def read_pedags(srv, name, title, kind, pedags) :
    """
    name:  'Mathématiques fondamentales'
    title: 'Semestre 6'
    kind:  'TD'
    pedags: {'Compter': [1.5, 2, 'John Duff'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}
    """
    
    for p in (pedag.Pedag(label, content) for (label, content) in pedags.items()) : 
        read_pedag(srv, name, title, kind, p)
    
def read_module(srv, name, title, shape):
    """
    name:  'Mathématiques fondamentales' 
    title: 'Semestre 5'
    shape: {'nom': 'Mathématiques fondamentales', 'ECTS': 5, 'shared': ['L3', 'Certificate'], 'CM': {'Intro': 3, 'Equadiff': [3, 'John Duff'], 'Entiers': [6, 'John Duff']}, 'TD': {'Compter': [1.5, 2, 'John Duff'], 'Compas': [1.5, 2, 'John Duff', 'Michel Choupauvert'], 'Règle': [3, 2]}, 'Exam': {'Ecrit': 2}}
    """
    for key in params.Params.pedag_kinds:
        if key in shape :
            read_pedags(srv, name, title, key, shape[key])

def read_respo(srv, respo) :
    if respo :
        for teacher in respo.who :
            if teacher not in srv :
                srv[teacher] = []
            srv[teacher].append((respo.name, 'Respo', '', respo.hetd_by_teacher, 'Admin'))
                
