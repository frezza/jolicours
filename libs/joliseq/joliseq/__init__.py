# coding: utf-8

import json
import os
from pathlib import Path


class Element:

    crossref_file = "tmp-crossrefs.txt"
    """
    contains "label class-abbrev" lines.
    """

    sequences_file = "tmp-sequences.json"
    """
    contains a dictionary such as :
    {'class1-abbref' : [abbrev1, abbrev2, abbrev3, ...],
    'class2-abbref' : [abbrev1, abbrev2, ...],
    ...
    }
    
    The list associated to a class is the classes that are supposed to occur after the key. The liste has no particular time order, it only means that each of its element should occur after the key.
    
    such 
    'class1-abbref' : [abbrev1, abbrev2, abbrev3, ...]
    information is what needs to be checked for a coherent shcedule.
    """

    timings_file = "tmp-timings.json"
    """
    {'module1-abbrev' : {'hpe-CM' : val, 'hpe-TD' : val, 'hpe-TP' : val, 'hpe-Projet': val, 'hpe-Exam' : val, 'hpe' : val, 'hee' : val},
    'module2-abbrev' : ...
    ...
    }
    """

    notag = "???"

    def set_dir(dirpath):
        Element.crossref_file = Path(dirpath) / Path(Element.crossref_file)
        Element.sequences_file = Path(dirpath) / Path(Element.sequences_file)
        Element.timings_file = Path(dirpath) / Path(Element.timings_file)

    # teacher =   'jean-pierre paul'
    #           | '@truc{machin}{chose}{bidule}'
    #
    # _format_teacher('jean-pierre paul', ...) --> jean-pierre paul'
    # _format_teacher('@truc{machin}{chose}{bidule}', 'cmd') --> 'truc'
    # _format_teacher('@truc{machin}{chose}{bidule}', 'fct') --> 'truc(machin, chose, bidule)'
    # _format_teacher('@truc{machin}{chose}{bidule}', 'who') --> 'bidule' (i.e. last arg).
    # _format_teacher('@truc{machin}{chose}{bidule}', 'who+') --> 'bidule (traduction['truc'] or 'truc')'
    # _format_teacher('@truc{machin}{chose}{bidule}', 'obj') --> ('truc', ['chose', 'bidule']).
    def _format_teacher(teacher, mode = 'cmd'):
        if teacher[0] != '@':
            return teacher
        elems = teacher[1:].split('{')
        command = elems[0]
        args = [e[:-1] for e in elems[1:]]
        if mode == 'cmd':
            return command
        if mode == 'fct':
            return command + '(' + ', '.join(args) + ')'
        if mode == 'who':
            return args[-1]
        if mode == 'who+':
            traduction = {'industriel': 'intervenant du milieu socio-économique',
                          'vacataire': 'vacataire du milieu académique'}
            if command in traduction:
                command = traduction[command]
            if len(args) > 0:
                return f'{args[-1]} ({command})'
            else:
                return command
        if mode == 'obj':
            return (command, args)
        raise ValueError(f'_format_teacher({teacher}, {mode}) : unknown mode "{mode}".')
    
    def __init__(
        self,
        kind,
        shape,
        color,
        classes,
        titre,
        intervenants,
        duree,
        nb_intervenants,
        forfait,
        face2face,
    ):
        classes.append(self)
        self.titre = titre
        self.kind = kind
        self.tag = None
        self.geode_rank = 0
        self.geode_tag = None
        self.shape = shape
        self.color = color
        self.label = None
        self.requirements = []
        if intervenants == None:
            self.qui = []
        elif isinstance(intervenants, str):
            self.qui = [intervenants]
        elif isinstance(intervenants, list):
            self.qui = intervenants
        else:
            raise ValueError(
                f"{self.kind} {self.titre} problème avec la déclaration des intervenants (None, str ou list)."
            )
        
        self.duree = duree
        self.idf = 0
        if nb_intervenants == None:
            self.nb_intervenants = len(self.qui)
        elif nb_intervenants < len(self.qui):
            raise ValueError(
                f'{self.kind} "{self.titre}" déclare trop de profs ({self.qui}, {nb_intervenants} max).'
            )
        else:
            self.nb_intervenants = nb_intervenants
        if self.nb_intervenants == 0:
            # on n'a déclaré ni profs ni un nombre d'intervenant.
            self.nb_intervenants = 1

        self.forfait = forfait
        self.face2face = face2face

    def has_teacher_like(self, teacher_kind):
        res = False
        for i in self.qui:
            if Element._format_teacher(i, 'cmd') == teacher_kind:
                return True
        return res

    def set_geode_tag(self, module_name):
        self.geode_tag = "{} - {} - S{}".format(module_name, self.kind, self.geode_rank)

    def __str__(self):
        qui = [Element._format_teacher(a, 'fct') for a in self.qui]
        for i in range(self.nb_intervenants - len(self.qui)):
            qui.append("???")
        who = ", ".join(qui)

        tag = self.tag
        return 'V{} [label=<<b>{}</b> ({}h) <br/> <i>{}</i> <br/>  <font point-size="10"><b>{}</b><br/><i>geode : {} - S{}</i></font>>, shape={}, fillcolor="{}", style="filled"]\n'.format(
            self.idf,
            str_to_dot(tag),
            self.duree,
            str_to_dot(self.titre),
            who,
            self.kind,
            self.geode_rank,
            self.shape,
            self.color,
        )

    def to_json(self):
        key = self.titre
        value = [self.duree, self.nb_intervenants] + self.qui
        if self.forfait is not None:
            value.append(f"forfait={self.forfait}")
        if self.face2face is not None:
            value.append(f"face2face={self.face2face}")
        return key, value


class Group:
    def __init__(self, groups, les_cours):
        groups.append(self)
        self.classes = les_cours
        self.idf = 0

    def __str__(self):
        result = "subgraph cluster_S{} {}\n".format(self.idf, "{")
        result += '      label=""\n'
        result += '      style="filled"\n'
        result += '      fillcolor="#dddddd"\n'
        for c in self.classes:
            result += "      " + str(c)
        result += "    {}\n".format("}")
        return result


class Ref:
    def __init__(self, refs, ref):
        refs.append(self)
        self.ref = ref
        self.idf = 0
        self.labels = None
        self.tag = Element.notag
        self.geode_tag = Element.notag

    def __str__(self):
        if self.labels and (self.ref in self.labels):
            self.tag, self.geode_tag = self.labels[self.ref]
            return 'V{} [label=<<i><b>{}</b></i><br/><i><font point-size="10">geode : {}</font></i>>, shape=oval, fillcolor="#ddddff", style="filled"]\n'.format(
                self.idf, self.tag, self.geode_tag
            )
        else:
            return 'V{} [label=<<font color="#ff0000"><b>???</b></font>>, shape=oval, fillcolor="#ddddff", style="filled"]\n'.format(
                self.idf
            )


class CM(Element):
    def __init__(self, classes, titre, intervenants, duree, nb_intervenants):
        super(CM, self).__init__(
            "CM",
            "rectangle",
            "#8080ff",
            classes,
            titre,
            intervenants,
            duree,
            nb_intervenants,
            None, None
        )


class TD(Element):
    def __init__(self, classes, titre, intervenants, duree, nb_intervenants):
        super(TD, self).__init__(
            "TD",
            "rectangle",
            "#ffe680",
            classes,
            titre,
            intervenants,
            duree,
            nb_intervenants,
            None, None
        )


class TP(Element):
    def __init__(self, classes, titre, intervenants, duree, nb_intervenants):
        super(TP, self).__init__(
            "TP",
            "rectangle",
            "#808080",
            classes,
            titre,
            intervenants,
            duree,
            nb_intervenants,
            None, None
        )


class Projet(Element):
    def __init__(self, classes, titre, intervenants, duree, nb_intervenants, forfait, face2face):
        super(Projet, self).__init__(
            "Projet",
            "rectangle",
            "#5aab83",
            classes,
            titre,
            intervenants,
            duree,
            nb_intervenants,
            forfait,
            face2face
        )


class Exam(Element):
    def __init__(self, classes, titre, intervenants, duree, nb_intervenants):
        super(Exam, self).__init__(
            "Exam",
            "hexagon",
            "#ff9961",
            classes,
            titre,
            intervenants,
            duree,
            nb_intervenants,
            None, None
        )


def str_to_md(msg):
    return msg.replace('&', '&amp;')

def str_to_dot(msg):
    return msg.replace('&', '&amp;')

class Module:
    def __init__(self, title, abbrev, ECTS, geode_title=None):
        if geode_title:
            self.geode_module_title = geode_title
        else:
            self.geode_module_title = title
        self.ECTS = ECTS
        self.title = title.replace("\n", "<br/>")
        self.sec_title = title.replace("\n", " ")
        self.english_title = 'not translated yet'
        self.abbrev = abbrev
        self.homework = None # Temps passé à la maison en + du présentiel, si différent du calcul par défaut.
        self.idf = 0
        self.cm_rank = 1
        self.td_rank = 1
        self.tp_rank = 1
        self.projet_rank = 1
        self.exam_rank = 1
        self.classes = {}
        self.groups = []
        self.refs = []
        self.current_section = ""
        self.links = []
        self.cursus = []
        self.requirements = {'global': [], 'CM': [], 'TD': [], 'TP': [], 'Projet': [], 'Exam': []}
        self.respo = None
        self.outcome = None
        self.description = None # string with @key
        self.used_title = {'CM': set(), 'TD': set(), 'TP': set(), 'Projet': set(), 'Exam': set()}
        self.comp = []
        self.bibitems = [] # (key, bibitem-string)

    def _check_title(self, kind, titre):
        if titre in self.used_title[kind]:
            raise ValueError(f'Module {self.abbrev} already has a {kind} named {titre}.')
        self.used_title[kind].add(titre)


    @property
    def competences(self):
        return self.comp

    @competences.setter
    def competences(self, value):
        if isinstance(value, list):
            self.comp = value
        elif isinstance(value, str):
            self.comp = [value]
        else:
            raise ValueError('For a module, competences must be a string (such as "c1 c2 c3") or a list of strings such as ["c1", "c2", "c3"].')
        
    
    @property
    def manager(self):
        return self.respo

    @manager.setter
    def manager(self, value):
        if isinstance(value, list):
            self.respo = value
        elif value is None:
            self.respo = None
        else:
            self.respo = [value]

    def share_with(self, *args):
        print('The method "share_with" is obsolete, use "in_cursus" instead.')
        self.in_cursus(*args)
        
    def in_cursus(self, *args):
        for arg in args:
            if isinstance(arg, str):
                self.cursus.append(arg)
            if isinstance(arg, list):
                self.cursus += arg

    # label mustn't contain space
    def label(self, elem, label):
        elem.label = label

    def elem_not_in_groups(self, elem):
        for g in self.groups:
            if elem in g.classes:
                return False
        return True

    def all_classes(self):
        res = []
        for _, sec in self.classes.items():
            for _, cs in sec.items():
                for c in cs:
                    res.append(c)
        for c in self.groups:
            res.append(c)
        return res

    def handle_all_labels(self):
        labels = dict()
        try:
            with open(Element.crossref_file, "r") as f:
                lines = [line.split("\n")[0] for line in f]
            for line in lines:
                words = line.split()
                label = words[0]
                tag = words[1]
                geode_tag = " ".join(words[2:])
                labels[label] = (tag, geode_tag)
        except:
            pass

        for secname, sec in self.classes.items():
            for kind, cs in sec.items():
                for i, c in enumerate(cs):
                    if c.label:
                        labels[c.label] = (c.tag, c.geode_tag)

        with open(Element.crossref_file, "w", encoding="utf8") as f:
            for label, (tag, geode_tag) in labels.items():
                f.write("{} {} {}\n".format(label, tag, geode_tag))

        return labels

    def abbrev_all(self):
        for secname, sec in self.classes.items():
            for kind, cs in sec.items():
                for i, c in enumerate(cs):
                    if len(cs) == 1:
                        number = ""
                    else:
                        number = "-{}/{}".format(i + 1, len(cs))
                    if secname == "":
                        c.tag = "{}-{}{}".format(self.abbrev, kind, number)
                    else:
                        c.tag = "{}-{}-{}{}".format(self.abbrev, secname, kind, number)

    def _write_requirements(self, desc):
        for k, v in self.requirements.items():
            if len(v) > 0:
                desc.write(f'Contraintes | module-{k} {", ".join(v)}\n')
        for kind in ['CM', 'TD', 'TP', 'Projet', 'Exam']:
            elems = []
            for section, classes in self.classes.items():
                elems += classes[kind]
            for e in elems:
                if len(e.requirements) > 0:
                    desc.write(f'Contraintes | {kind}-{e.geode_rank} {", ".join(e.requirements)}\n')
            
        
        
    def _write_content_description(self, kind, desc):
        elems = []
        for section, classes in self.classes.items():
            elems += classes[kind]
        if len(elems) == 0:
            return
        desc.write(f"\n**{kind}s**\n\n")
        for e in elems:
            desc.write('  1. {} ({:0.1f} h) \n'.format(str_to_md(e.titre), e.duree))
        desc.write(' \n')
        
    def _write_description(self, md_file):

        bibkeys = {elem[0]: (value+1) for value, elem in enumerate(self.bibitems)}

        if self.description is not None:
            for k, v in bibkeys.items():
                self.description = self.description.replace(f'@{k}', f'[ref. {v}]')

        
        desc = open(md_file, "w", encoding="utf8")
        desc.write(f"\n#### {str_to_md(self.sec_title)} ({self.abbrev})\n\n")

        if self.description is not None:
            desc.write(str_to_md(self.description))
        else:
            desc.write("Description à rédiger.")
        desc.write("\n\n")

        count = self.count_hpe_hee()
        desc.write(f"{self.abbrev} | {count['hpe']} HPE, {self.ECTS} ECTS\n")
        desc.write(f":---|:---\n")
        
        msg = []
        if count["hpe-CM"] > 0:
            msg.append(f"**CM** = {count['hpe-CM']}h")
        if count["hpe-TD"] > 0:
            msg.append(f"**TD** = {count['hpe-TD']}h")
        if count["hpe-TP"] > 0:
            msg.append(f"**TP**={count['hpe-TP']}h")
        if count["hpe-Projet"] > 0:
            msg.append(f"**Projet**={count['hpe-Projet']}h")
            if count["hpe-Projet-face2face"] > 0:
                msg.append(f" (dont {count['hpe-Projet-face2face']}h en face à face)")
            
        if count["hpe-Exam"] > 0:
            msg.append(f"**Exam**={count['hpe-Exam']}h")
        msg = ", ".join(msg)
        desc.write(f"Volumes | {msg}\n")
        
        desc.write(f"Title | {self.english_title}\n")
            


        desc.write(f"Evaluation | {', '.join([str_to_md(c.titre) for c in self.all_classes() if isinstance(c, Exam)])}\n")

        desc.write("Acquis d'apprentissage | ")
        if self.outcome == None:
            desc.write('à rédiger\n')
        else:
            desc.write(f'{self.outcome}\n')
        
        if self.respo is not None and len(self.respo) > 0:
            respos = [Element._format_teacher(t, 'who') for t in self.respo]
            resp = ", ".join(respos)
        else:
            resp = "à déterminer"
        desc.write(f"Responsable(s) | {resp}\n")

        qui = set()
        for c in self.all_classes():
            if isinstance(c, CM) or isinstance(c, TD) or isinstance(c, TP) or isinstance(c, Projet) or isinstance(c, Exam):
                who = [buddy for buddy in c.qui if (buddy[:8] != 'forfait=' and buddy[:9] != 'face2face')]
                qui = qui.union(set(who))
        qui = [Element._format_teacher(t, 'who+') for t in qui]
        desc.write(f"Intervenant(s) | {', '.join(qui)}\n")

        if len(self.cursus) < 2:
            desc.write(f"Partage | non \n")
        else:
            desc.write(f"Partage | oui \n")

        desc.write(f"Cursus | {', '.join(self.cursus)}\n")
        desc.write(f"Compétences | {',  '.join(self.comp)}\n")
        


        for k, v in count.items():
            if len(k) > 4 and k[:4] == 'f2f-':
                desc.write(f'Face-à-face avec {k[4:]} | {v}h\n')
        
        self._write_requirements(desc)

        for k, v in self.bibitems:
            desc.write(f"ref. {bibkeys[k]} | {v} \n")
            
            
        
        desc.write('\n')

        self._write_content_description('CM', desc)
        self._write_content_description('TD', desc)
        self._write_content_description('TP', desc)
        self._write_content_description('Projet', desc)
        self._write_content_description('Exam', desc)
        
        desc.write("\n")
        desc.write(f"[comment]: # ( {count['hpe-CM']} {count['hpe-TD']} {count['hpe-TP']} {count['hpe-Projet']} {count['hpe-Projet-face2face']} {count['hpe-Projet-forfait']} {count['hpe-Exam']} {self.homework} ")
        desc.write(' )\n')
        
        desc.write("\n\n")

        desc.close()

    def write(self,
              dot_file=Path("sequencement.dot"),
              json_file=Path("module.json"),
              md_file=Path("description.md"),
              ):
        self.abbrev_all()
        labels = self.handle_all_labels()
        dotfile = open(dot_file, "w", encoding="utf8")
        dotfile.write("digraph All {}\n".format("{"))

        dotfile.write("  subgraph cluster_W {\n")
        dotfile.write(
            '    label = <<font point-size="50">{} ({})</font>>\n'.format(
                str_to_dot(self.title), str_to_dot(self.abbrev)
            )
        )

        jolicours = {"nom": self.abbrev, "ECTS": self.ECTS}

        if len(self.cursus) > 0:
            jolicours["cursus"] = self.cursus
            jolicours["shared"] = len(self.cursus) > 1

        for c in self.all_classes():
            if self.elem_not_in_groups(c):
                dotfile.write("    {}\n".format(c))
            if isinstance(c, Element) and c.duree != 0:
                if c.kind not in jolicours:
                    jolicours[c.kind] = {}
                key, value = c.to_json()
                jolicours[c.kind][key] = value

        with open(json_file, "w", encoding="utf-8") as jsonfile:
            json.dump(jolicours, jsonfile, ensure_ascii=False)

        for c in self.refs:
            c.labels = labels
            dotfile.write("    {}\n".format(c))

        dotfile.write("  }\n")

        for l in self.links:
            dotfile.write("  V{}".format(l[0].idf))
            for s in l[1:]:
                dotfile.write(" -> V{}".format(s.idf))
            dotfile.write("\n")

        dotfile.write("}\n")

        self.update_sequence_file()
        self.update_timings_file()

        dotfile.close()

        self._write_description(md_file)

    def update_sequence_file(self):
        try:
            succession = json.load(open(Element.sequences_file, "r"))
        except:
            succession = {}

        for c in self.all_classes():
            if not isinstance(c, Group):
                if c.geode_tag not in succession:
                    succession[c.geode_tag] = []

        for c in self.refs:
            succession[c.geode_tag] = []

        for seq in self.links:
            if len(seq) > 1:  # should be true....
                for idx, first in enumerate(seq[:-1]):
                    if first.geode_tag != Element.notag:
                        for second in seq[idx + 1 :]:
                            if second.geode_tag not in succession[first.geode_tag]:
                                succession[first.geode_tag].append(second.geode_tag)

        open(Element.sequences_file, "w", encoding="utf8").write(
            json.dumps(succession, ensure_ascii=False)
        )

    def count_hpe_hee(self):
        categories = ['doctorant', 'industriel', 'vacataire']
        count = {
            "hpe-CM": 0,
            "hpe-TD": 0,
            "hpe-TP": 0,
            "hpe-Projet": 0,
            "hpe-Projet-forfait": 0,
            "hpe-Projet-face2face": 0,
            "hpe-Exam": 0
        }
        for cat in categories:
            count[f'f2f-{cat}'] = 0
            
        for c in self.all_classes():
            f2f = None
            if isinstance(c, CM):
                count["hpe-CM"] += c.duree
                f2f = c.duree
            if isinstance(c, TD):
                count["hpe-TD"] += c.duree
                f2f = c.duree
            if isinstance(c, TP):
                count["hpe-TP"] += c.duree
                f2f = c.duree
            if isinstance(c, Projet):
                count["hpe-Projet"]         += c.duree
                count["hpe-Projet-face2face"] += c.face2face
                count["hpe-Projet-forfait"] += c.forfait
                f2f = c.face2face
            if isinstance(c, Exam):
                count["hpe-Exam"] += c.duree
                f2f = c.duree
            if f2f is not None:
                for cat in categories:
                    if c.has_teacher_like(cat):
                        count[f'f2f-{cat}'] += f2f
        total = (
            count["hpe-CM"]
            + count["hpe-TD"]
            + count["hpe-TP"]
        )
        count["hpe"] = total + count["hpe-Projet"] + count["hpe-Exam"]
        count["hee"] = count["hpe"] / 0.6
        count["hpe-face2face"] = total + count["hpe-Projet-face2face"]
        count["hpe-forfait"] = total + count["hpe-Projet-forfait"]
        return count

    def update_timings_file(self):
        try:
            timings = json.load(open(Element.timings_file, "r"))
        except:
            timings = {}
        timings[self.abbrev] = self.count_hpe_hee()
        open(Element.timings_file, "w", encoding="utf8").write(
            json.dumps(timings, ensure_ascii=False)
        )

    # Gets the dict for a section.
    def section_dict(self, secname):
        if secname not in self.classes:
            self.classes[secname] = {
                "CM": [],
                "TD": [],
                "TP": [],
                "Projet": [],
                "Exam": [],
            }
        return self.classes[secname]

    def section(self, secname=""):
        self.current_section = secname

    def current_dict(self):
        return self.section_dict(self.current_section)

    def cm(self, titre, intervenants, duree=1.5, nb_intervenants=None):
        self._check_title('CM', titre)
        e = CM(
            self.current_dict()["CM"],
            titre,
            intervenants,
            duree,
            nb_intervenants
        )
        e.idf = self.idf
        self.idf += 1
        e.geode_rank = self.cm_rank
        e.set_geode_tag(self.geode_module_title)
        self.cm_rank += 1
        return e

    def td(self, titre, intervenants, duree=1.5, nb_intervenants=None):
        self._check_title('TD', titre)
        e = TD(
            self.current_dict()["TD"],
            titre,
            intervenants,
            duree,
            nb_intervenants
        )
        e.idf = self.idf
        self.idf += 1
        e.geode_rank = self.td_rank
        e.set_geode_tag(self.geode_module_title)
        self.td_rank += 1
        return e

    def tp(self, titre, intervenants, duree=3, nb_intervenants=None):
        self._check_title('TP', titre)
        e = TP(
            self.current_dict()["TP"],
            titre,
            intervenants,
            duree,
            nb_intervenants
        )
        e.idf = self.idf
        self.idf += 1
        e.geode_rank = self.tp_rank
        e.set_geode_tag(self.geode_module_title)
        self.tp_rank += 1
        return e

    def projet(self, titre, intervenants, duree, nb_intervenants=None, forfait=None, face2face=None):
        if forfait == None:
            raise ValueError(f'Projet "{titre}" : forfait argument is missing')
        if face2face == None:
            raise ValueError(f'Projet "{titre}" : face2face argument is missing')
        self._check_title('Projet', titre)
        e = Projet(
            self.current_dict()["Projet"],
            titre,
            intervenants,
            duree,
            nb_intervenants,
            forfait,
            face2face
        )
        e.idf = self.idf
        self.idf += 1
        e.geode_rank = self.projet_rank
        e.set_geode_tag(self.geode_module_title)
        self.projet_rank += 1
        return e

    def exam(self, titre, intervenants, duree, nb_intervenants=None):
        self._check_title('Exam', titre)
        e = Exam(
            self.current_dict()["Exam"],
            titre,
            intervenants,
            duree,
            nb_intervenants
        )
        e.idf = self.idf
        self.idf += 1
        e.geode_rank = self.exam_rank
        e.set_geode_tag(self.geode_module_title)
        self.exam_rank += 1
        return e

    def exam_ecrit(self, intervenants, duree, nb_intervenants=None):
        return self.exam('Examen écrit', intervenants, duree, nb_intervenants)

    def exam_oral(self, intervenants, duree, nb_intervenants=None):
        return self.exam('Examen oral', intervenants, duree, nb_intervenants)

    def group(self, *args):
        if len(args) == 1 and isinstance(args[0], list):
            cours_list = args[0]
        elif len(args) > 1:
            cours_list = list(args)
        else:
            raise ValueError(f'module.group(...) : bad arguments')
        e = Group(self.groups, cours_list)
        e.idf = self.idf
        self.idf += 1

    def seq(self, *args):
        if len(args) == 1 and isinstance(args[0], list):
            self.links.append(args[0])
        elif len(args) > 1:
            self.links.append(list(args))
        else:
            raise ValueError(f'module.seq(...) : bad arguments')

    def ref(self, ref):
        e = Ref(self.refs, ref)
        e.idf = self.idf
        self.idf += 1
        return e


# This is for specific gitlab_ci intergration
def filenames_from_argv(dot_file, json_file, md_file, argv):
    if len(argv) > 1:
        build_dir = Path(argv[1])
        Element.set_dir(build_dir)
        json_path = Path(argv[2])
        if json_path.suffix == ".json":
            json_file = json_path
            dot_file = build_dir / Path(dot_file)
            md_file = build_dir / Path(md_file)
        else:
            json_file = json_path / Path(json_file)
            dot_file = json_path / Path(dot_file)
            md_file = json_path / Path(md_file)
    return dot_file, json_file, md_file
