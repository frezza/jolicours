# coding: utf-8

import json
import os
from pathlib import Path
import datetime
import xlsxwriter
import copy

def feries_fr(annee, color, lundi_paques = None, jeudi_ascension = None,
              lundi_pentecote = None, alsace_moselle = False):
    res = [(datetime.date(annee,  1,  1), "jour de l'an"),
           (datetime.date(annee,  5,  1), "fête du travail"),
           (datetime.date(annee,  5,  8), "victoire 1945"),
           (datetime.date(annee,  7, 14), "fête nationale"),
           (datetime.date(annee,  8, 15), "assomption"),
           (datetime.date(annee, 11,  1), "toussaint"),
           (datetime.date(annee, 12, 25), "noël")]
    if lundi_paques == None:
        raise ValueError('Spécifiez le lundi de Pâques')
    if jeudi_ascension == None:
        raise ValueError("Spécifiez le jeudi de l'ascension")
    if lundi_pentecote != None:
        res.append((lundi_pentecote, "lundi pentecôte"))
    res.append((lundi_paques, "lundi pâques"))
    res.append((jeudi_ascension, "jeudi ascension"))
    if alsace_moselle:
        res.append((lundi_paques - datetime.timedelta(days=3), 'vendredi saint')) 
        res.append((datetime.date(annee, 12, 26), 'st étienne'))
    res.sort(key = lambda x : x[0])
    return [(copy.deepcopy(color), copy.deepcopy(text), lambda x, d = copy.deepcopy(day) : x != d) for (day, text) in res]



not_we = lambda day : day.weekday() not in [5, 6]

not_monday    = lambda day : day.weekday() != 0
not_tuesday   = lambda day : day.weekday() != 1
not_wednesday = lambda day : day.weekday() != 2
not_thursday  = lambda day : day.weekday() != 3
not_friday    = lambda day : day.weekday() != 4
not_saturday  = lambda day : day.weekday() != 5
not_sunday    = lambda day : day.weekday() != 6

def out_of(unauthorized_days):
    """
    returns f, such as f(day) is True if day is out of the list unauthorized_days.
    """
    return lambda day, f = unauthorized_days: day not in f

def not_in(start, end):
    """
    returns f, such as f(day) is True if day is out of the ]start, end[ time range.
    """
    first = start + datetime.timedelta(days=1)
    def outside(day, duration = (end - first).days):
        d = (day - first).days
        return d < 0 or d >= duration
    return outside

class Calendar:
    def __init__(self, start_time):
        """
        filter is [(color, text, f), (color, text, f), ....]
        where f(day) is False if the day is off.
        """
        self._filters     = []
        self._annotations = []
        self.colors = set()
        self._now = start_time # The next day to consider (may not be free).
        self.days = []         # The days (i.e. (col, text, day) list).


    @property
    def filters(self):
        raise RuntimeError('calendar.filters is private')

    @filters.setter
    def filters(self, the_filters):
        self._filters = the_filters
        for (c, _, _) in the_filters:
            self.colors.add(c)
    
    @property
    def annotations(self):
        raise RuntimeError('calendar.annotations is private')
    
    @annotations.setter
    def annotations(self, the_annotations):
        self._annotations = the_annotations
        for (c, _, _) in the_annotations:
            self.colors.add(c)
    

    def __str__(self):
        res = ''
        for col, text, day in self.days:
            res += f'  {col} : {day.isoformat()}, {text}\n'
        res += f'now : {self._now.isoformat()}'
        return res
    
    def __iadd__(self, nb):
        """
        Skip nb **available** days. 
        """
        if nb <= 0:
            raise ValueError(f'nb = {nb}, should be > 0')

        filtered = True
        while filtered:
            filtered = False
            for color, text, fltr in self._filters:
                if not fltr(self._now):
                    self._new_day(color, text)
                    filtered = True
                    self._now += datetime.timedelta(days = 1)
                    break
        # we are at the first available timepoint

        for _ in range(nb-1):
            self._now += datetime.timedelta(days = 1)
            filtered = True
            while filtered:
                filtered = False
                for color, text, fltr in self._filters:
                    if not fltr(self._now):
                        self._new_day(color, text)
                        filtered = True
                        self._now += datetime.timedelta(days = 1)
                        break
            # we are at the next available timepoint      
                
        return self

    def _is_free(self):
        for color, text, fltr in self._filters:
            if not fltr(self._now):
                return False
        return True
        

    @property
    def now(self):
        return self._now

    @now.setter
    def now(self, day):
        """
        day : a **future** datetime.date, telling when is the current day to
        be considered. 

        """

        if day == None:
           day = self._now
        
        if (day - self._now).days < 0:
            raise ValueError(f'now = {day} is a forbidden jump into the past from {self._now}.')

        while self._now != day:
            for color, text, fltr in self._filters:
                if not fltr(self._now):
                    self._new_day(color, text)
                    break
            self._now += datetime.timedelta(days = 1)

    def _new_day(self, color, text):
        for col, txt, fltr in self._annotations:
            if not fltr(self.now):
                self.days.append((col, txt, self.now))
                return
        self.days.append((color, text, self.now))

    def emplace(self, color, text, nb=1):
        """
        emplace nb days in the calendar.
        """
        if nb <= 0:
            return
        self.colors.add(color)
        for _ in range(nb):
            self += 1
            self._new_day(color, text)
            self._now = self._now + datetime.timedelta(days = 1)
            
            
    def emplace_week(self, color, text, nb=1):
        """
        emplaces the next nb week.
        """
        if nb <= 0:
            return
        if nb == 1:
            self.colors.add(color)
            wd = self._now.weekday()
            if wd != 0: # not a monday
                nb_jumps = 7 - wd # nb of days to skip.
                for _ in range(nb_jumps):
                    for ccolor, ttext, fltr in self._filters:
                        if not fltr(self.now):
                            self._new_day(ccolor, ttext)
                            break
                    self._now = self._now + datetime.timedelta(days = 1)
            for _ in range(7):
                not_filtered = True
                for ccolor, ttext, fltr in self._filters:
                    if not fltr(self.now):
                        self._new_day(ccolor, ttext)
                        not_filtered = False
                        break
                    if not_filtered:
                        self._new_day(color, text)
                self._now = self._now + datetime.timedelta(days = 1)
        else:
            self.emplace_week(color, text,      1)
            self.emplace_week(color, text, nb - 1)
        

    def _incoming(self, day_num, nb):
        """
        Go to next nb incoming day_num (1=monday, 2 = tuesday, ...).
        nb >= 1 is required.
        If current day is already a daynum, it is the first incoming day.
        """
        if nb <= 0:
            raise ValueError(f'nb = {nb}, should be > 0')
        n = self._now.weekday()
        if n == day_num:
            target = self.now
        elif day_num > n:
            target = self.now + datetime.timedelta(days = day_num - n)
        else:
            target = self.now + datetime.timedelta(days = day_num + 7 - n)
        if nb > 1:
            target = target + datetime.timedelta(days = 7 * (nb - 1))
        self.now = target

    def _free_incoming(self, day_num, nb):
        """
        as _incomming, but once done, it skips non free days until some free day_num is found.
        """
        self._incoming(day_num, nb)
        while not self._is_free():
            self += 1
            self._incoming(day_num, 1)
    
            

    def next_monday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(0, nb)
        
    def next_tuesday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(1, nb)
        
    def next_wednesday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(2, nb)
        
    def next_thursday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(3, nb)
        
    def next_friday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(4, nb)
        
    def next_saturday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(5, nb)
        
    def next_sunday(self, nb=1):
        """
        specification of _incoming
        """
        self._incoming(6, nb)

    def next_free_monday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(0, nb)
        
    def next_free_tuesday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(1, nb)
        
    def next_free_wednesday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(2, nb)
        
    def next_free_thursday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(3, nb)
        
    def next_free_friday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(4, nb)
        
    def next_free_saturday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(5, nb)
        
    def next_free_sunday(self, nb=1):
        """
        specification of _free_incoming
        """
        self._free_incoming(6, nb)

    def _excel_year_origin(self, year):
        y = year - self.min_year
        return  y, (y // self.width) * self.block_height + self.offset, (y %  self.width) * self.block_width + self.offset
    
    def _excel_year_frame(self, year):
        y, year_line, year_col = self._excel_year_origin(year)
        
        self.worksheet.merge_range(year_line, year_col+1, year_line, year_col+12, year, self.formats['year-title'])
        self.worksheet.write(year_line    , year_col, None, self.formats['year-day'])
        self.worksheet.write(year_line + 1, year_col, None, self.formats['year-day'])
        for day in range(1, 32):
            self.worksheet.write(year_line + 1 + day, year_col, day, self.formats['year-day'])
        for i, m in enumerate(['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']):
            self.worksheet.write(year_line + 1, year_col + 1 + i, m, self.formats['year-month'])

    def _excel_pos(self, day):
        y, year_line, year_col = self._excel_year_origin(day.year)
        lbase = year_line + 1
        cbase = year_col 
        return lbase + day.day, cbase + day.month


    def to_excel(self, filename):
        self.width = 2
        self.offset = 1
        self.block_width  = 12+1 + self.offset
        self.block_height = 31+2 + self.offset
        
        self.workbook = xlsxwriter.Workbook(filename)
        self.worksheet = self.workbook.add_worksheet('calendar')

        self.formats = {
            'year-title' : self.workbook.add_format({'bg_color' : '#555555', 'color' : '#ffffff', 'bold' : True, 'align' : 'center'}),
            'year-month' : self.workbook.add_format({'bg_color' : '#cccccc', 'align' : 'center'}),
            'year-day' : self.workbook.add_format({'bg_color' : '#cccccc', 'align' : 'center'})
        }
        self.formats.update({col: self.workbook.add_format({'bg_color' : col, 'align' : 'center'}) for col in self.colors})

        if len(self.days) > 0:
            _, _, min_day = self.days[0]
            _, _, max_day = self.days[-1]
            self.min_year, self.max_year = min_day.year, (max_day.year + 1)

            for year in range(self.min_year, self.max_year):
                self._excel_year_frame(year)

            for col, text, day in self.days:
                l, c = self._excel_pos(day)
                self.worksheet.write(l, c, text, self.formats[col])
            
        
        self.workbook.close()
        print(f'file "{filename}" generated.')
        

    
        

    
