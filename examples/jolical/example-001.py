# coding: utf-8

import jolical
from datetime import date

# Filters are a way to declare available days.

# First, let us declare that available days are not in French bank days.
filters = jolical.feries_fr(2023, '#ffff55', alsace_moselle = True,
                            lundi_paques    = date(2023, 4, 10),
                            jeudi_ascension = date(2023, 5, 18),
                            lundi_pentecote = date(2023, 5, 29))
filters += jolical.feries_fr(2024, '#ffff55', alsace_moselle = True,
                            lundi_paques    = date(2024, 4, 1),
                            jeudi_ascension = date(2024, 5, 9),
                            lundi_pentecote = date(2024, 5, 20))

ponts = [date(2023, 9, 15)]

# Second, let us exclude some other days from the available ones.
filters += [('#ffdd55', 'vacances noël', jolical.not_in(date(2023, 12, 20), date(2024, 1, 3))), # Xmas holiday (fake)
            ('#ffdd55', 'pont', jolical.out_of(ponts)),
            ('#ccccff', 'we',   jolical.not_we),
            ('#99dd99', 'free', jolical.not_thursday)]

# The defines the calendar silulation.
calendar  = jolical.Calendar(date(2023, 9, 1))  # It starts at Friday sept 1st.

# We set filters (no filters is en empty list)
calendar.filters = filters

print(calendar, end = '\n\n')

calendar += 3 # we skip 3 free dates. Due to week-ends we are at wednesday 6th .
print(calendar, end = '\n\n')

calendar += 1 # Due to freed thursdays, we ar at friday 8th.
print(calendar, end = '\n\n')

calendar.next_saturday(); # We go to next saturday, i.e. the 9th.
print(calendar, end = '\n\n')

calendar.next_saturday(); # We go to next saturday, i.e... still the 9th.
print(calendar, end = '\n\n')

calendar += 1 # We go to next free day, i.e. monday 11th
print(calendar, end = '\n\n')

calendar += 2 # We reach the next 2 free days : monday (current) and tuesday 12th.
print(calendar, end = '\n\n')

calendar.next_free_friday() # Next friday is not free, so this will lead us to 22nd.
print(calendar, end = '\n\n')


# From now, let is remove the free days. All the filters are accessible, so this can easily be done.
# the calendar.filters attribute is exactly our filters variable.
calendar.filters = [(color, text, filtering_function) for (color, text, filtering_function) in calendar.filters if text != 'free']

calendar.now = date(2024, 12, 31) # We go to then end of 2024.

calendar.to_excel('example-001.xlsx')
