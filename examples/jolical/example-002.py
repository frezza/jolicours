# coding: utf-8

import jolical
from datetime import date

col_bank_days = '#ffdd55'
col_holidays  = '#00aa00'
col_freed     = '#ccffcc'
col_we        = '#ccccff'
col_semester  = '#aa0000'
col_extra     = '#aa00aa'

filters = jolical.feries_fr(2023, col_bank_days, alsace_moselle = True,
                            lundi_paques    = date(2023, 4, 10),
                            jeudi_ascension = date(2023, 5, 18),
                            lundi_pentecote = date(2023, 5, 29))
filters += jolical.feries_fr(2024, col_bank_days, alsace_moselle = True,
                            lundi_paques    = date(2024, 4, 1),
                            jeudi_ascension = date(2024, 5, 9),
                            lundi_pentecote = date(2024, 5, 20))

# week ends
filters.append((col_we, 'we', jolical.not_we))

ponts = [date(2024, 5, 10)]
filters.append((col_holidays, 'pont', jolical.out_of(ponts)))

filters += [(col_holidays, 'Toussaint', jolical.not_in(date(2023, 10, 21), date(2023, 11,  6))), 
            (col_holidays, 'Xmas',      jolical.not_in(date(2023, 12, 23), date(2024,  1,  8))),
            (col_holidays, 'Winter',    jolical.not_in(date(2024,  2, 24), date(2024,  3, 11))),
            (col_holidays, 'Spring',    jolical.not_in(date(2024,  4, 20), date(2024,  5,  6)))]

# All thursdays are freed.
filters.append((col_freed, 'free thursday', jolical.not_thursday))


calendar         = jolical.Calendar(date(2023, 9, 4))
calendar.filters = filters

calendar.emplace(col_extra, 'extra', 3) # 3 days placed.

# Let us start 13 days from incoming monday
calendar.next_monday()
calendar.emplace(col_semester, 'semester 1', 13)

# we need 5 days off, and then restart at next available monday.
calendar += 5
calendar.next_free_monday()

# We are ready for next semester
calendar.emplace(col_semester, 'semester 2', 13)

# we need 5 days off, and then restart at next available monday.
calendar += 5
calendar.next_free_monday()

# We are ready for next semester
calendar.emplace(col_semester, 'semester 3', 13)





calendar.to_excel('example-002.xlsx')
