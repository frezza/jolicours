# coding: utf-8

import jolical
from datetime import date

col_bank_days = '#ffdd55'
col_we        = '#ccccff'
col_week      = '#ffccff'
col_busy      = '#dd5555'
col_annote    = '#55dd55'

filters = [(col_we, 'we', jolical.not_we),
           (col_bank_days, 'off', jolical.out_of([date(2023, 1, 11),
                                                  date(2023, 2,  6),
                                                  date(2023, 2, 14),
                                                  date(2023, 2, 17)]))]

annotations = [(col_annote, 'birthday', jolical.out_of([date(2023, 1, 21)])),
               (col_annote, 'telework', jolical.out_of([date(2023, 1, 10),
                                                        date(2023, 1, 17)]))]

calendar             = jolical.Calendar(date(2023, 1, 1))
calendar.filters     = filters
calendar.annotations = annotations

# Annotation behaves as filters, but they only label the day. The
# anotated day is still considered as available, as opposed to
# filtered out days.

calendar.next_monday()
calendar.emplace_week(col_week, 'week A')

calendar.emplace(col_busy, 'busy', 3)


calendar.emplace_week(col_week, 'week B', 2)
            
calendar.next_wednesday()
calendar.emplace_week(col_week, 'week C', 2)



calendar.now = date(2023, 3, 31)
calendar.to_excel('example-004.xlsx')

