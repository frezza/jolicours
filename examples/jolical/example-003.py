# coding: utf-8

import jolical
from datetime import date

col_bank_days = '#ffdd55'
col_we        = '#ccccff'
col_week      = '#ffccff'

filters = [(col_we, 'we', jolical.not_we),
           (col_bank_days, 'off', jolical.out_of([date(2023, 2,  6),
                                                  date(2023, 2, 14),
                                                  date(2023, 2, 17)]))]

calendar         = jolical.Calendar(date(2023, 1, 1))
calendar.filters = filters

calendar.next_monday()
calendar.emplace_week(col_week, 'week A')

calendar.next_wednesday()
calendar.emplace_week(col_week, 'week B', 2)
            
calendar.next_wednesday()
calendar.emplace_week(col_week, 'week C', 2)



calendar.now = date(2023, 3, 31)
calendar.to_excel('example-003.xlsx')

