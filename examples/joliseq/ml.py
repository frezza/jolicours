# coding: utf-8

# Quand vous ferez votre propre description de cours inspirée de ce
# fichier, il faudra nommer le fichier sequencement.py.

# Les lignes commençant par # sont des commentaires

# Laissez ça, cela nous permettra de générer des schémas pour les
# secrétaires qui feront les emplois du temps.
import joliseq

# Variables utiles, pour s'éviter de trop écrire après.
jf = "Jérémy Fix"
hfb = "Hervé Frezza-Buet"
phd1 = "@doctorant{Jean-Pierre Paul}{Loria}"
phd2 = "@doctorant"

# joliseq travaille sur des fichiers mis en communs. On peut définir
# le dossier où ils seront écris ou lus. Par défaut, ce dossier est le
# dossier courant. Ici, on définit (inutilement donc) ce dossier comme
# le dossier courant.
joliseq.Element.set_dir('.')

# Mettez ici le titre de votre cours, i.e. du module dont vous êtes
# responsable. Le deuxième argument est un tag court pour les
# désignations abrégées de votre cours. Le dernier argument est
# le nombre d'ECTS.
module = joliseq.Module("Apprentissage Automatique", "ML", 5)
module.english_title = "Machine Learning"

# Vous pouvez dire ici à quelle(s) formation(s) appartient ce module
# Ces formations sont simplement référencées par un nom.
module.in_cursus('all')
module.in_cursus('Saclay', 'Rennes')
module.in_cursus(['Metz', 'Casablanca', 'Pékin'])

# On peut également préciser un(e) responsable de cours (on peut
# mettre une liste).
module.manager = [hfb, jf]

# On peut associer une description globale au cours. @xxx est une note biblio.
module.description = 'Ce cours @toto présente les concepts généraux du Machine Learning@ml. On commencera par aborder...'
module.bibitems = [('toto', 'Les blaques de Toto, 2021, puf'),
                   ('ml', "Le poly d'apprentissage automatique de SDI-M")]

# On peut aussi lister les compétences (leur code) abordées.
module.competences = ['C1', 'C2', 'C3.1']

# On peut aussi lister les contraintes (e.g. de salle)
module.requirements['global'] = ['Tous les cours le lundi'] # ATTENTION: pas de virgules dans les strings
module.requirements['TP'] = ['En salle machine', "l'après-midi"] # idem 'CM', 'TD', 'Projet', 'Exam'
# Pour les contrainte pour un acte pédagogique particulier, voir tp_intro_2

# On peut décrire des acquis d'apprentissage
module.outcome = 'Ce cours permet de comprendre le fonctionnement interne des algorithmes de ML'

# On peut décrire le temps passé en plus à la maison, si jamais
# celui-ci diffère d'une estimation par défaut.
module.homework = 20 # heures

# L'idée est de décomposer votre cours en sections. Ce n'est pas
# obligatoire, mais c'est mieux, car les thèmes (i.e. les sections)
# vous permettent de vous y retrouver dans l'emploi du temps une fois
# que tout y est placé.

# Dans ce cours de ML, il y a les sections "Intro", "Ensembles", "VQ",
# "SVM". Décrivons-en les contenu section par section.

module.section("Intro")
# Ecrire module.section() pour revenir hors sections, à la racine.
# Les élements de même type (CM, TD, TP, ...)  au sein d'une section X
# sont notés CM-X-1/n, CM-X-2/n, etc. Il faut donc mettre des noms
# très courts, une sorte de tag, en guise de nom de section.


"""
Ce cours aborde les notions générales d'échantillonnage des données, 
bla bla bla
bla bla bla """
cm_intro1 = module.cm("Overview of machine learning", hfb)
cm_intro2 = module.cm("Dimensionality", jf)
cm_intro3 = module.cm("Principles of data conditioning", jf)
module.group(cm_intro2, cm_intro3) # on demande que ces cours soient consécutifs si possible.

# Attention ! Pour une même catégorie de cours (CM, TD, ...), les titres doivent être tous différents.
# Une exception est levée en cas de duplication de titres.

# Ceci est un cours avec 2 enseignants définis
tp_intro1 = module.tp("Dimensionality reduction", [jf, hfb])

# Pour les intervenants d'un cours, on peut préciser combien il en faut, sans forcément tous les nommer.
tp_intro2 = module.tp("Data", jf, nb_intervenants=2) # On meut mettre None à la place de jf pour ne définir aucun des 2 intervenants requis.
tp_intro2.requirements = ['Dans la grande salle', 'Prévoir maquettes'] # Contraintes pour cet acte pédagoque précisément.

# Ici, on dit comment les choses doivent se suivre.
module.seq(cm_intro1, cm_intro2, cm_intro3) # on passe une liste...
module.seq(cm_intro2, tp_intro1, tp_intro2) # ... ou directement les éléments.
module.seq(cm_intro3, tp_intro2)

# On fait de même pour les autres sections.

module.section("Ensembles")
cm_ens1 = module.cm("Boosting", hfb)
cm_ens2 = module.cm("Decision trees", hfb)
tp_ens1 = module.tp("Bagging", [jf, hfb])
module.seq(cm_intro3, cm_ens1, cm_ens2, tp_ens1)

module.section("VQ")
cm_vq1 = module.cm("Introduction to vector quantization", hfb)
cm_vq2 = module.cm("Vector quantization algorithms", hfb)
tp_vq1 = module.tp("Self-Organizing maps", [jf, phd2])
module.seq(cm_intro3, cm_vq1, cm_vq2, tp_vq1)

module.section("SVM")
cm_svm1 = module.cm("Introduction to Support Vector Machines", hfb)
cm_svm2 = module.cm("Kernel methods", hfb)
tp_svm1 = module.tp("Digit recognition with SVMs", [phd1, hfb])
module.seq(cm_intro3, cm_svm1, cm_svm2, tp_svm1)

module.section() # On revient hors section.
td   = module.td("Annals", hfb)
exam = module.exam_ecrit(hfb, duree=3)
module.seq(tp_intro2, td, exam)
module.seq(tp_ens1, td)
module.seq(tp_vq1, td)
module.seq(tp_svm1, td)

module.section() # On revient hors section.
# Posons des séances de projet. On met des séances de 4h. Elles sont payés au forfait comme .5 heures pour un enseignant. On met 8 de ces séances. Le temps en face à face est, lui de 1 heure par séance.

projets = [module.projet(f'appli-{i}', [hfb, jf], duree=4, forfait=.5, face2face=1) for i in range(1, 9)]
module.seq(projets)


# Cette partie montre l'utilisation de labels et de référence (comme
# en latex), si l'on veut exprimer des contraintes d'enchaînement avec
# des modules générés par d'autres fichiers .py comme celui-ci.

# You can label your classes, in order to reference them in other
# files. Do not use space in the label.
module.label(td, "annals-ml")

# In some other file (let us do it here, even if it is silly to do
# both label and reference in the same file), you can define a
# reference to that label.
ref1 = module.ref("annals-ml")

# This is a reference to something that does not exist
ref2 = module.ref("foo-bar-foo")

# We put it as a prerogative to the first class, which is silly as
# well.
module.seq(ref1, cm_intro1)
module.seq(ref2, cm_intro1)


# We generate files here.
module.write(dot_file = 'ml.dot', json_file = 'ml.json', md_file = 'ml.md')
