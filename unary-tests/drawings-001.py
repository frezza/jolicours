# coding: utf-8

import jolicours

tracks_dict = {
    'M-Poudlard': {'name': 'Master de Nécromancie de Poudlard', 'symbol': 'Pdl',  'color': [0.1, 0.8, 0.1], 'HPE': 50},
    'M-Teissier': {'name': 'Msc Elizabeth Teissier',            'symbol': 'Eliz', 'color': [0.1, 0.5, 0.8], 'HPE': 60}
}

tarot = {
    'nom':    'Tarot',
    'ECTS':   2,
    'tracks': {'M-Poudlard': True, 'M-Teissier': False},
    'CM':     {'Le jeu': 3, 'Tarot occulte': [6, 'Nostradamus']},
'TD':     {'Cartes': [1.5, 2, 'Nostradamus', 'Elizabeth Tessier'], 'Entrailles': [3, 4]},
    'TP':     {'Divination': [5, 2, 'Nostradamus', 'Elizabeth Tessier']},
    'Exam':   {'Oral': 0.5, 'Ecrit': 3}
}

belote = {
    'nom': 'Belote',
    'CM':  {'Règles': 1.5}
}

le_ciel = {
    'nom':    'Le ciel',
    'tracks': {'M-Poudlard': False},
    'CM':     {'Les étoiles': 3, 'Planètes': 6}
}

astrologie_chinoise = {
    'nom':    'Astrologie chinoise',
    'ECTS':   3,
    'shared': ['Pipeaulogie', 'Flan'],
    'tracks': {'M-Teissier': True},
    'CM':     {'Dragon': 3, 'Rat': 3},
    'TP':     {'Amour': 6, 'Chance': 3}
}

astrologie_occidentale = {
    'nom': 'Astrologie occidentale',
    'ECTS': 2,
    'shared': 'Pipeaulogie',
    'CM':   {'Verseau': 3, 'Capricorne': 3, 'Balance': 3},
    'TP':   {'Amour': 6, 'Chance': 3, 'Argent': [6, 3]}}

astrologie_indienne = {
    'nom': 'Astrologie indienne',
    'shared': 'Flan',
    'tracks': {'M-Teissier': True},
    'TD': {'Recherche doc.': 12}
}

conclusion = {
    'nom':  'Conclusion',
    'ECTS': 1,
    'CM':   {'Séminaire': [1, 'Elizabeth Tessier']},
    'TP':   {'Charlatanisme': [6, 2, 'Nostradamus', 'Elizabeth Tessier']}
}

shapes = [
    ((0, 0),       (0, 0),       'Cartomancie', ['CM: 10.5, TD: 4.5, TP: 5.0, Exam: 3.5', 'HPE: 23.5, HETD: 44.2, ECTS: 2.0', 'no share']),
    ((3, 0),       (13.0, 12.4), 'Cartomancie', tarot),
    ((3, 12.6),    (13.0, 14.5), 'Cartomancie', belote),
    ((15.5, 0),    (0, 0),       'Thème astral', ['CM: 25.0, TD: 12.0, TP: 30.0, Exam: 0.0', 'HPE: 40.0, HETD: 97.5, ECTS: 4.0', '(shared: HPE: 51.0, HETD: 70.5, ECTS: 5.0)']),
    ((18.5, 0),    (28.5, 12.4), 'Thème astral', le_ciel),
    ((18.5, 12.6), (28.5, 25.0), 'Thème astral', astrologie_chinoise),
    ((29.0, 12.6), (39.0, 33.4), 'Thème astral', astrologie_occidentale),
    ((39.5, 12.6), (49.5, 29.2), 'Thème astral', astrologie_indienne),
    ((18.5, 33.6), (28.5, 41.8), 'Thème astral', conclusion)
]
    
jolicours.drawings.draw(shapes, 42, 50, 'drawings-001.pdf', tracks_dict)
print('drawings-001.pdf generated.')
