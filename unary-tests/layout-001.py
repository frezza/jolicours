import jolicours

# coding: utf-8
data_list = [
    [
        'Cartomancie',
        [
            {
                'nom': 'Tarot',
                'ECTS': 2,
                'tracks': {'M-Poudlard': True, 'M-Teissier': False},
                'CM': {'Le jeu': 3, 'Tarot occulte': [6, 'Nostradamus']},
                'TD': {'Cartes': [1.5, 2, 'Nostradamus', 'Elizabeth Tessier'], 'Entrailles': [3, 4]},
                'TP': {'Divination': [5, 2, 'Nostradamus', 'Elizabeth Tessier']},
                'Exam': {'Oral': 0.5, 'Ecrit': 3}
            },
            {
                'nom': 'Belote',
                'CM': {'Règles': 1.5}
            }
        ]
    ],
    [
        'Thème astral',
        [
            {
                'nom': 'Le ciel',
                'tracks': {'M-Poudlard': False},
                'CM': {'Les étoiles': 3, 'Planètes': 6}
            },
            [
                {
                    'nom': 'Astrologie chinoise',
                    'ECTS': 3, 'tracks': {'M-Teissier': True},
                    'shared': ['Pipeaulogie', 'Flan'],
                    'CM': {'Dragon': 3, 'Rat': 3},
                    'TP': {'Amour': 6, 'Chance': 3}
                },
                {
                    'nom': 'Astrologie occidentale',
                    'ECTS': 2,
                    'shared': 'Pipeaulogie',
                    'CM': {'Verseau': 3, 'Capricorne': 3, 'Balance': 3},
                    'TP': {'Amour': 6, 'Chance': 3, 'Argent': [6, 3]}
                },
                {
                    'nom': 'Astrologie indienne',
                    'shared': 'Flan',
                    'tracks': {'M-Teissier': True},
                    'TD': {'Recherche doc.': 12}
                }
            ],
            {
                'nom': 'Conclusion',
                'ECTS': 1,
                'CM': {'Séminaire': [1, 'Elizabeth Tessier']},
                'TP': {'Charlatanisme': [6, 2, 'Nostradamus', 'Elizabeth Tessier']}
            }
        ]
    ]
]


shapes, tfin, endline = jolicours.layout.place_all(data_list)
for begin, end, title, data in shapes:
    ymin, xmin = begin
    ymax, xmax = end
    print(f'x:[{xmin}, {xmax}] y:[{ymin}, {ymax}]', title, data)
        
        
