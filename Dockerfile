FROM	python:latest
RUN pip3 install setuptools
RUN	apt update; apt-get install -y python3-setuptools
RUN	git clone https://gitlab-research.centralesupelec.fr/frezza/jolicours.git ; cd jolicours/libs/jolicours/ ; python3 setup.py install; cd ../joliseq ; python3 setup.py install ; cd ../jolical ; python3 setup.py install 
RUN	apt update; apt-get install -y graphviz libgraphviz-dev
RUN	pip install pygraphviz
RUN	apt update; apt-get install -y pandoc
RUN	pip3 install pandocfilters
RUN	pip3 install pygments
RUN	pip3 install numpy matplotlib
ENV	PYTHONPATH /jolicours/libs/joliseq


